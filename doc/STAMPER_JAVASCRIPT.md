@page JAVASCRIPT_STAMPER
\section proxyStamper Proxy Stamper : Contrat de service


Ci-dessous, le contrat de service à utiliser dans les formalités développées sous NASH afin de signer électroniquement un ou une liste de documents à destination d'une autorité compétente.

Deux Process sont à mettre en place dans une formalité autonome pour assurer la fonctionalité de signature électronique.

Les paramètres nécessaires pour pouvoir signer un document avec le proxy Stamper : 

| **Paramètre**   	| **Description**                                             						| **O/F** 	| **Exemple** 			|
|-------------------|-----------------------------------------------------------------------------------|-----------|-----------------------|
| civility    	    | Civilité	                                                                        | O       	| Madame, Monsieur	    |
| lastName  		| Nom										            							| O       	| Nom 					|
| fistName		  	| Prénom											           						| O       	| Prénom   				|
| email				| Rôle du destinataire						 			                    		| O       	| adresse@yopmail.com	|
| phone		    	| Numéro de téléphone			                              						| O       	| 0033612345678       	|
| files		  		| Document à signer																	| O			| 						|


~~~~~~~~~~~~~{.xml}
 	<process id="stampingInit" type="proxy.out" input="/step01/data.xml" output="proxy-calling.xml" script="hangout.js"></process>

	<process id="stampingReturn" type="proxy.in" input="#stampingInit" output="data.xml" script="hangin.js"></process>
~~~~~~~~~~~~~

\subsection proxyOut Proxy Out : Décrochage d'une formalité autonome 

Le proxy.out permet de générer le fichier de paramétrage de redirection vers le proxy de signature électronique :

~~~~~~~~~~~~~{.js}
	var docs = $step01.grp01.files;
	_log.info("Documents are {}", docs);
	
	var files = [];
	for (var i = 0; i < docs.length; ++i) {
	    files.push({
	        'document': docs[i],
	        'zoneId': 'id' + i
	    });
	}
	var nfo = nash.hangout.stamp({
	    'files' : files,
	    'civility' : $step01.grp01.civility,
	    'lastName' : $step01.grp01.lastName,
	    'firstName' : $step01.grp01.firstName,
	    'email' : $step01.grp01.email,
	    'phone' : $step01.grp01.phone
	});
	
	return nfo;
~~~~~~~~~~~~~

\subsection proxyIn Proxy In : Racrochage vers une formalité autonome

Le proxy.in permet le traitement du résultat de retour de la signature électronique. Ce process récupère le fichier résultat alimenté par le proxy et le donne en entrée au script "hangin.js".

~~~~~~~~~~~~~{.js}
	var proxyResult = _INPUT_.proxyResult;
	
	<group id="proxyResult" label="Proxy Result">
	<data id="status" label="Status du proxy" />
	<data id="message" label="Status du proxy" />
	<data id="files" label="Liste des fichiers" type="FileReadOnly" />
	</group>
	
	return spec.create({
	    id : 'proxy',
	    label : 'Résultat de l\'appel au proxy',
	    groups : [ spec.createGroup({
	        id : 'result',
	        label : 'Résultat de l\'appel au proxy',
	        data : [ spec.createData({
	            id : 'status',
	            label : 'Status',
	            type : 'String',
	            value : proxyResult.status
	        }), //
	        spec.createData({
	            id : 'message',
	            label : 'Message',
	            type : 'String',
	            value : proxyResult.message
	        }), //
	        spec.createData({
	            id : 'files',
	            label : 'Documents',
	            type : 'FileReadOnly',
	            value : proxyResult.files
	        }) ]
	    }) ]
	});
~~~~~~~~~~~~~


\subsection conception Algorithme
Voir fichier "conception-proxy-stamper.svg".









