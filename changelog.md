# Changelog
Ce fichier contient les modifications techniques du module.

## [2.16.1.0] - 2020-09-11

###  Ajout

- Modifier le logo de la bannière

__Properties__ :

### Modification

| fichier   |      nom      | nouvelle valeur |
|-----------|:-------------:|----------------:|
| application.properties | ui.theme.default | default |

## [2.15.1.0] - 2020-06-02

### Evolution
Migration Dictao v2 vers v3

### Modification

- __Properties__ 

| fichier   |      nom      | Nouvelle valeur pour la plateforme de Dictao de TEST |
|-----------|:-------------:|-----------------------------------------------------:|
| application.properties | ct.dictao.dtp.frontoffice.backEndBaseUrl | https://sig-1s.qa.trust.idemia.io |
| application.properties | ct.dictao.dtp.frontoffice.endUserBaseUrl | https://sig-1.qa.trust.idemia.io |
| application.properties | ct.dictao.dtp.ws.url | https://sig-1s.qa.trust.idemia.io/dtp/frontoffice/ws/v5u1 |
| application.properties | ct.dictao.dtp.euapp.url | https://sig-1.qa.trust.idemia.io/dtp-ui/guichet_entreprises/scnge/# |
| application.properties | ct.dictao.ssl.keyStore | Chemin absolu du nouveau fichier keystore (.p12) |
| application.properties | ct.dictao.ssl.trustStore | Chemin absolu du nouveau fichier truststore (.jks) |

## [2.14.3.0] - 2020-04-08
### Evolution
API de suppression des sessions

### Ajout

- __Properties__ 

## [2.12.4.0] - 2019-11-05
### Evolution
Ajouts de liens dans le menu

### Ajout

- __Properties__ 

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | ui.dashboard.url | URL publique du Dashboard Guichet-Partenaires  | ${DASHBOARD_GP_UI_EXT} |
| application.properties | welcome.public.url | URL publique de Welcome Guichet-Partenaires  | ${WELCOME_GP_UI_EXT} |

## [2.12.1.0] - 2019-07-16
### Evolutiono
Mise en place du bouchon pour le module de paiement

### Ajout

- __Properties__ 


Concerne uniquement les applications : proxy-payment

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | paybox.payment.mock.url | URL publique du proxy pour bouchonner les paiements | https://payment.${env}.guichet-entreprises.fr/public/paybox/payment/mock |

## [2.11.8.0] - 2019-07-233
### Corrections
Erreur lors de la récupération des comptes VAD sous forms2 MINE-864

## [2.11.5.1] - 2019-06-266
### Corrections
- Montant paiement global en erroné: DEST-875

## [2.11.5.0] - 2019-06-188

### Corrections
- Reçu de paiement taille des caractères non conforme:  DEST-835

- Intégrer l'étape de paiement dans les FA de modification DEST-860

## [2.11.4.3] - 2019-06-077

### Corrections
- Wording du bouton Modifier (dossier FORMS 1) : DEST-856

## [2.11.4.0] - 2019-06-066

###  User Storiess

- Bouton "Retour vers votre dossier" après paiement : DEST-834

## [2.11.3.2] - 2019-05-244

###  User Storiess

- Générer une preuve de paiement en cas de paiement partiel :  DEST-759

###  Modificationn

- __Properties__ :

Concerne uniquement les applications : proxy-payment

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | paybox.action.plus | URL Paybox pour effectuer les paiements | https://preprod-ppps.paybox.com/PPPS.php |

## [2.11.2.2] - 2019-05-100

- Projet : [Proxy](https://tools.projet-ge.fr/gitlab/apps/proxy)

###  Ajoutt
- Mise en place du décrochage vers Paybox
- Mise en place d'un paiement effectué avec succès (demande d'autorisation + paiement par produit)

- __Properties__ :

Concerne uniquement les applications : proxy-payment

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | proxy.mode | Mode du proxy | proxy.live |
| application.properties | paybox.key.secret | Clé paybox | a28d6bee-2618-4e73-950e-c4f669eeb653 |
| application.properties | paybox.callback.done.url | URL retour des paiements effectués | https://payment.${env}.guichet-entreprises.fr/public/paybox/done |
| application.properties | paybox.callback.cancel.url | URL retour des paiements annulés | https://payment.${env}.guichet-entreprises.fr/public/paybox/cancel |
| application.properties | paybox.callback.denied.url | URL retour des paiements refusés | https://payment.${env}.guichet-entreprises.fr/public/paybox/denied |
| application.properties | paybox.callback.ipn.url | URL retour IPN | https://payment.${env}.guichet-entreprises.fr/public/paybox/ipn |
| application.properties | paybox.authorization.url | URL Paybox des demandes d'autorisations | https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi |
| application.properties | paybox.public.key | Chemin absolu de la clé pubique Paybox | /srv/ge/gent/payment/ssl/paiement/certificat/pubkey.pem |
| application.properties | paybox.action.plus | URL Paybox pour effectuer les paiements | https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi |
| application.properties | ws.directory.url | URL privée de Directory d'accès aux référentiels | http://directory-ws.${env}.guichet-partenaires.loc:12030/api |
| application.properties | ws.directory.repository.baseurl | URL privée de Directory d'accès aux repository | http://directory-ws.${env}.guichet-partenaires.loc:12030/api/private/v1 |

### Modificationn

Concerne uniquement les applications : proxy-payment

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | ct.authentification.url.exclude | Pattern d'exclusion des URL nécessitant une authentification | ^.*/(status|paybox/ipn|(css|fonts|images|js)/.*)$ |

### Suppressionn

## Liens

[2.11.2.0](https://tools.projet-ge.fr/gitlab/apps/proxy/tags/proxy-2.11.2.2)

