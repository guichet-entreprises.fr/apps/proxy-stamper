define([ 'jquery' ], function ($) {

    $(document).on('click', 'a[data-toggle="confirm"]', function(evt) {
        var btn = $(this), url = btn.attr('href'), dlg = $('#confirmModal');

        evt.preventDefault();

        $('.modal-header > .modal-title', dlg).html(btn.data('confirm-title'));
        $('.modal-body', dlg).html(btn.data('confirm-content'));

        dlg.on('click', 'button[role="validate"]', function(evt) {
            document.location = url;
        }).modal('show');
    });

});