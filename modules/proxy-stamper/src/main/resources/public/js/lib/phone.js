/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Dependencies :
// - 'jquery'
// - 'lib/jqueryMaskPlugin'
// - 'intlTelInput'
// - 'awesome-phonenumber'
define([ 'jquery', 'lib/jqueryMaskPlugin', 'intl-tel-input', 'awesome-phonenumber' ], function ($) {
    /**
     * Initializes the phone field.
     * @param field the jquery div object around the 3 inputs
     * @param opts the options
     * @returns nothing
     */
    function initialize(field, opts) {
        var country = field.find('input[name$="country"]');
        var e164 = field.find('input[name$="e164"]');
        var international = field.find('input[name$="international"]');
        var initialCountryExists = false;
        if (opts.initialCountry) {
            var countries = $.fn.intlTelInput.getCountryData();
            for (var i = 0; i < countries.length; ++i) {
                if (countries[i].iso2.toLowerCase() == opts.initialCountry
                        .toLowerCase()) {
                    initialCountryExists = true;
                    break;
                }
            }
        }
        international.mask('W', {
            translation : {
                'W' : {
                    pattern : /[\+\-\(\)\./ 0-9]/,
                    optional : true,
                    recursive : true
                }
            }
        });
        international
                .intlTelInput({
                    initialCountry : (initialCountryExists
                            && opts.initialCountry && opts.initialCountry
                            .toLowerCase())
                            || 'fr',
                    nationalMode : true,
                    preferredCountries : []
                });
        international.on("countrychange", function(e, countryData) {
            var iso2Country = international
                    .intlTelInput("getSelectedCountryData").iso2;
            if (!international.is(":focus") && international.val().length > 0
                    && iso2Country) {
                var pn = PhoneNumber(international.val(), iso2Country);
                if (pn.getRegionCode() != iso2Country.toUpperCase()) {
                    var nationalNumber = pn.getNumber("national");
                    if (nationalNumber) {
                        pn = PhoneNumber(nationalNumber, iso2Country);
                        international.val(pn.getNumber("international"));
                    }
                }
                international.trigger("change");
            }
            if (opts.displayExample) {
                international.attr("placeholder", PhoneNumber.getExample(
                        iso2Country).getNumber("national"));
            }
        });
        international.on("change", function() {
            var iso2Country = international
                    .intlTelInput("getSelectedCountryData").iso2;
            country.val(iso2Country);
            if (international.val().length > 0) {
                var pn = PhoneNumber(international.val(), iso2Country);
                if (pn.isValid()) {
                    international.val(pn.getNumber("international"));
                }
                e164.val(pn.getNumber('e164'));
            }
        });
        international.trigger("countrychange");
        return field;
    }

    /**
     * Validates the phone field.
     * @param field the jquery div object around the 3 inputs
     * @param opts the options
     * @returns an object with the status of the validation and an optional error message
     */
    function validate(field, opts) {
        var country = field.find('input[name$="country"]').val();
        var e164 = field.find('input[name$="e164"]').val();
        var international = field.find('input[name$="international"]').val();
        var pn = PhoneNumber(international, country);
        var validation = {
            validated : (international.length == 0 || (country.length > 0 && pn
                    .isValid()))
        };
        if (validation.validated) {
            validation.value = {
                country : country,
                e164 : e164,
                international : international
            };
        } else {
            var pnJSON = pn.toJSON();
            if (pnJSON.possibility == "invalid-country-code") {
                validation.message = 'The country code of this phone number is invalid.';
            } else if (pnJSON.possibility == "too-long") {
                validation.message = 'This phone number is too long.';
            } else if (pnJSON.possibility == "too-short") {
                validation.message = 'This phone number is too short.';
            } else {
                validation.message = 'This phone number format is not valid.';
            }
        }
        return validation;
    }

    var actions = {
        'initialize' : initialize,
        'validate' : validate
    };

    $.fn.phone = function(cmd, opts) {
        if (typeof cmd !== 'string') {
            opts = cmd;
            cmd = 'initialize';
        }
        if (actions[cmd]) {
            return actions[cmd](this, opts || {});
        } else {
            return this;
        }
    };

    return $;

});
