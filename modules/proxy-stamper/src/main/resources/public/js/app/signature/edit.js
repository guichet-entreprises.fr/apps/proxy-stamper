/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
requirejs([ 'jquery', 'lib/modal', 'lib/i18n', 'lib/phone', 'bootstrap', 'bootstrap-fileinput', 'lib/fr'], function($, modal, i18n) {

    $("#input-id").fileinput({
    	maxFileCount: 2,
    	overwriteInitial: true,
    	fileActionSettings: {
            showUpload: false
    	},
        language: 'fr',
        showUpload: false,
        allowedFileExtensions: ["jpg", "png", "gif","pdf"]
    });
    
    $('.view-pdf').on('click',function(){
        var pdf_link = $(this).attr('href');
        //var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'" width="100%" frameborder="0"></iframe></div>'
        var iframe = '<embed src="'+pdf_link+'" frameborder="0" width="100%">'
        var defaults = {
                title:'Signature du document',
                message: iframe,
                closeButton:true,
                closeButtonTitle: 'Fermer',
                scrollable:false
            };
        modal(defaults);
        return false;
    });

    function validate() {
        var block = $("#phone");
        var validation = block.phone("validate");
        block.children('.type-errors-list').remove();
        if (validation.validated === false) {
            var displayedMessage = i18n(validation.message || 'This value is not valid.');
            block.append($('<ul class="type-errors-list">').append($("<li>").text(displayedMessage)));
            return false;
        }
        return true;
    }

    $("#phone").phone().find('input[name$="international"]').change(function() {
        validate();
    });
	
//	$('.file-drop-zone').on('drop',function(e){
//		console.log(e);
//		var file = e.originalEvent.dataTransfer;
//        console.log(file);
//		console.log($("#input-id"));
//		$('#input-id').prop('files',file.files);
//	});

    $('#btn-document-submit').on('click', function() {
        if (!validate()) {
            return false;
        }
    });

});
