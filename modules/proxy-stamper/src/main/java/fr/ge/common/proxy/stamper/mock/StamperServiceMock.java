/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.proxy.stamper.mock;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import org.apache.cxf.helpers.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.data.service.IProxySessionDataService;
import fr.ge.common.proxy.stamper.IStamperService;
import fr.ge.common.proxy.stamper.bean.StamperConfigurationBean;

/**
 * Stamper proxy mock.
 *
 * @author aolubi
 */
public class StamperServiceMock implements IStamperService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StamperServiceMock.class);

    /** Le service dictao. */
    @Autowired
    private StamperConfigurationBean stamperConfigurationBean;

    @Autowired
    private IProxySessionDataService proxySessionDataService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String dictaoCall(final ProxySessionBean session) {
        final String txId = this.createTransaction();
        this.proxySessionDataService.addTransaction(session.getToken(), txId);
        return "redirect:" + this.stamperConfigurationBean.getUrlStamper() + "/public/dictao/callback?status=OK&login=mock_" + txId + "_" + "accessId_&offer=formalite";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] getDocument(String txId, String documentName) {
        try {
            return Optional.ofNullable(this.proxySessionDataService.findSessionByTransactionId(txId)) //
                    .filter(s -> null != s) //
                    .map(s -> s.getParameterAsBytes("files.1.doc")) //
                    .map(c -> {
                        return this.signMock(c);
                    }).orElse(IOUtils.readBytesFromStream(this.getClass().getResourceAsStream("/images/specimen.pdf")));
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Sign document.
     *
     * @param doc
     *            the doc
     * @return the byte[]
     * @throws IOException
     */
    private byte[] signMock(byte[] resourceAsBytes) {
        PDDocument doc = null;
        try {
            doc = PDDocument.load(resourceAsBytes);
        } catch (Exception e) {
            return null;
        }
        final PDAcroForm form = doc.getDocumentCatalog().getAcroForm();
        final PDField field = form.getField("signature");
        if (null != field) {
            try {
                field.setValue("Mock signature");
            } catch (final IOException ex) {
                throw new TechnicalException(String.format("Error while setting [%s] : %s", field.getFullyQualifiedName(), ex.getMessage()), ex);
            }
        }
        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); BufferedOutputStream buffer = new BufferedOutputStream(out)) {
            doc.save(buffer);
            doc.close();
            buffer.flush();
            return out.toByteArray();
        } catch (final Exception ex) {
            LOGGER.warn("Saving PDF document : {}", ex.getMessage(), ex);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAndFinishUser(String login, String accessId) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void finishAndArchive(String txId, ProxySessionBean proxySession, String offer) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancelTransaction(String txId) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createTransaction() {
        return UUID.randomUUID().toString();
    }

}
