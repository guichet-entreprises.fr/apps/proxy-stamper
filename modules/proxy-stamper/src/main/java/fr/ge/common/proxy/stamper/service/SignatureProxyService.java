/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.proxy.stamper.service;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.ge.common.proxy.bean.ProxyResultBean;
import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.resultcode.IProxyResultCodeEnum;
import fr.ge.common.proxy.stamper.error.StamperResultCodeEnum;
import fr.ge.common.proxy.support.AbstractProxyService;

/**
 * Cart Service as the payment proxy's default proxy service.
 * {@see AbstractProxyService}
 *
 * @author jpauchet
 */
@Service
public class SignatureProxyService implements AbstractProxyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignatureProxyService.class);

    /**
     * Get the model and session, fill the model's attributes using the
     * payment's information that are present in the session given as a
     * parameter then return the user to the payment form so he can proceed with
     * the payment
     * 
     * @param model
     *            : the model that will be used for the payment form
     * @param session
     *            : the session that contains the payment's information
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Override
    public String run(final Model model, final ProxySessionBean session) {
        LOGGER.debug("Get proxy session parameters to redirect upload CNI page");
        return "redirect:/public/document?token=" + session.getToken();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProxyResultBean getResult(final ProxySessionBean session) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<? extends IProxyResultCodeEnum> getResultCodeClass() {
        return StamperResultCodeEnum.class;
    }
}
