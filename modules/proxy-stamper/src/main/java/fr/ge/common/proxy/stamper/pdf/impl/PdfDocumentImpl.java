/**
 * 
 */
package fr.ge.common.proxy.stamper.pdf.impl;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.springframework.stereotype.Service;

import fr.ge.common.proxy.stamper.pdf.IPdfDocument;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author bsadil
 *
 */
@Service
public class PdfDocumentImpl implements IPdfDocument {
    // TODO LAB mettre des logs dans cette classe
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean image(final String extension) {
        return Arrays.asList(new String[] { "image/jpg", "image/png", "image/gif", "image/jpeg" }).contains(extension);
    }

    public boolean acceptExtensions(final String extension) {
        return Arrays.asList(new String[] { "application/pdf", "image/jpg", "image/png", "image/gif", "image/jpeg" }).contains(extension);
    }

    public PDDocument build(byte[] asBytes) {

        if (null == asBytes) {
            throw new TechnicalException(String.format("Unable to load a null resource."));
        }

        final PDDocument doc = new PDDocument();

        try {
            final BufferedImage image = ImageIO.read(new ByteArrayInputStream(asBytes));
            final PDPage page = new PDPage();
            doc.addPage(page);

            final PDImageXObject imageObject = JPEGFactory.createFromImage(doc, image, .95F, 150);

            final PDRectangle portrait = PDRectangle.A4;

            if (imageObject.getWidth() > imageObject.getHeight()) {
                final PDRectangle landscape = new PDRectangle(portrait.getLowerLeftY(), portrait.getLowerLeftX(), portrait.getHeight(), portrait.getWidth());
                page.setMediaBox(landscape);
            } else {
                page.setMediaBox(portrait);
            }

            final PDRectangle displayBox = page.getCropBox();
            final Dimension newSize = fitness(new Dimension(imageObject.getWidth(), imageObject.getHeight()), new Dimension((int) displayBox.getWidth(), (int) displayBox.getHeight()));

            try (PDPageContentStream pageContentStream = new PDPageContentStream(doc, page)) {
                pageContentStream.drawImage( //
                        imageObject, //
                        (float) ((displayBox.getWidth() - newSize.getWidth()) / 2), //
                        (float) ((displayBox.getHeight() - newSize.getHeight()) / 2), //
                        (float) newSize.getWidth(), //
                        (float) newSize.getHeight() //
                );
            }
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Error while appending resource \"%s\"", asBytes), ex);
        }

        return doc;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] append(byte[] fileToMerge, byte[] origin, String extension) {

        PDFMergerUtility merger = new PDFMergerUtility();

        if (null == origin) {
            throw new TechnicalException("Unable to merge from a null resource.");
        }
        try {
            if (acceptExtensions(extension)) {
                if (image(extension)) {
                    PDDocument doc = build(fileToMerge);
                    fileToMerge = save(doc);
                }
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                out.reset();
                merger.setDestinationStream(out);
                merger.addSource(new ByteArrayInputStream(fileToMerge));

                merger.addSource(new ByteArrayInputStream(origin));

                merger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
                out.flush();
                out.close();

                return out.toByteArray();
            }
            return null;
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Error while appending resource \"%s\"", origin), ex);
        }
    }

    private byte[] save(PDDocument doc) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); BufferedOutputStream buffer = new BufferedOutputStream(out)) {
            doc.save(buffer);
            doc.close();
            buffer.flush();
            return out.toByteArray();
        } catch (final Exception ex) {
            throw new TechnicalException(String.format("Unable to save PDF resource : %s", ex.getMessage()), ex);
        }
    }

    /**
     * Fitness.
     *
     * @param source
     *            the source
     * @param box
     *            the box
     * @return the dimension
     */
    private static Dimension fitness(final Dimension source, final Dimension box) {
        final double widthRatio = source.getWidth() / box.getWidth();
        final double heightRatio = source.getHeight() / box.getHeight();
        final double ratio = Math.max(widthRatio, heightRatio);

        return new Dimension((int) (source.getWidth() / ratio), (int) (source.getHeight() / ratio));
    }

}
