/**
 * 
 */
package fr.ge.common.proxy.stamper.pdf;

import org.apache.pdfbox.pdmodel.PDDocument;

/**
 * @author bsadil
 *
 */
public interface IPdfDocument {

    /**
     * Accept.
     *
     * @param extension
     *            the extension
     * @return true, if successful
     */
    boolean image(String extension);

    /**
     * @param fileToMerge
     *            file to merge
     * @param origin
     *            cerfa from nash
     * @param extensionOfFile
     *            extension of file to merge
     * @return byte of pdf mered
     */
    byte[] append(final byte[] fileToMerge, final byte[] origin, String extensionOfFile);

    boolean acceptExtensions(final String extension);

    PDDocument build(byte[] asBytes);

}
