/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.proxy.stamper.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.ge.common.proxy.bean.ProxyResultBean;
import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.data.bean.ProxySessionParameterBean;
import fr.ge.common.proxy.data.service.IProxySessionDataService;
import fr.ge.common.proxy.stamper.IStamperService;
import fr.ge.common.proxy.stamper.bean.Phone;
import fr.ge.common.proxy.stamper.constante.CodeRetourDictaoEnum;
import fr.ge.common.proxy.stamper.constante.IProxyConstante;
import fr.ge.common.proxy.stamper.constante.ISignatureConstante;
import fr.ge.common.proxy.stamper.pdf.IPdfDocument;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.core.exception.TechniqueException;

/**
 * Controller managing Dictao callback.
 *
 * @author aolubi
 */
@Controller
public class StamperController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StamperController.class);

    @Autowired
    private IProxySessionDataService proxySessionDataService;

    @Value("${proxy.mode:#{null}}")
    private String proxyMode;

    @Autowired
    private IPdfDocument pdfDocument;

    /** Pattern international phone. **/
    private final Pattern PATTERN_INTERNATIONAL_PHONE = Pattern.compile("^([0-9]?){6,14}[0-9]$");

    @Autowired
    @Qualifier("dictao.mock")
    private IStamperService dictaoMock;

    @Autowired
    @Qualifier("dictao.live")
    private IStamperService dictaoLive;

    /**
     * Return the stamper service (live or mock).
     * 
     * @return
     */
    public IStamperService getService() {
        if (StringUtils.isNotEmpty(this.proxyMode) && "proxy.mock".equals(this.proxyMode)) {
            return this.dictaoMock;
        }
        return this.dictaoLive;
    }

    /**
     * Inits.
     *
     * @return the template name
     */
    @RequestMapping(value = "/public/document", method = RequestMethod.GET)
    public String init(final Model model, @RequestParam("token") final String token) {
        model.addAttribute("token", token);
        model.addAttribute("URL_REDIRECT_UI", "/");
        final ProxySessionBean proxy = this.proxySessionDataService.findByToken(token);
        if (proxy != null) {
            final String phoneStr = proxy.getParameterAsString("phone");
            if (StringUtils.isNotEmpty(phoneStr) && PATTERN_INTERNATIONAL_PHONE.matcher(phoneStr).matches()) {
                Phone phone = new Phone();
                phone.setInternational(phoneStr);
                model.addAttribute("phone", phone);
            }
            Arrays.stream(new String[] { "lastName", "firstName", "URL_REDIRECT_UI_FIRST_PAGE" }) //
                    .filter(attr -> StringUtils.isNotEmpty(proxy.getParameterAsString(attr))) //
                    .forEach(attr -> model.addAttribute(attr, proxy.getParameterAsString(attr)));
        }

        return "signature/edit/main";
    }

    /**
     * Download.
     *
     * @param token
     *            the token
     * @return the response entity
     */
    @RequestMapping(value = "/public/document/download/{token}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download(@PathVariable final String token, @DefaultValue("false") @RequestParam("modal") final Boolean showModal) {
        final ProxySessionBean proxy = this.proxySessionDataService.findByToken(token);

        byte[] pdf;
        if (proxy != null) {
            pdf = proxy.getParameterAsBytes("files.1.doc");
        } else {
            pdf = null;
        }
        if (pdf == null) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/pdf");

        if (showModal) {
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + "document.pdf");
            return new ResponseEntity<>(pdf, headers, HttpStatus.CREATED);
        }
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachement; filename=" + "document.pdf");
        return new ResponseEntity<>(pdf, headers, HttpStatus.OK);
    }

    /**
     * Uploads a document.
     *
     * @param request
     *            the request
     * @param file
     *            the file
     * @return the redirect string
     */
    @RequestMapping(value = "/public/document/upload", method = RequestMethod.POST)
    public String upload(final HttpServletRequest request, @RequestParam("input-file[]") final List<MultipartFile> file, @RequestParam("phone.international") final String phoneInternational,
            @RequestParam("token") final String token) {
        String redirect = null;
        try {
            final ProxySessionBean proxy = this.proxySessionDataService.findByToken(token);
            if (proxy != null) {
                byte[] pdf = proxy.getParameterAsBytes("files.1.doc");
                byte[] pdfmerged = null;
                for (MultipartFile f : file) {
                    pdfmerged = this.pdfDocument.append(f.getBytes(), pdf, f.getContentType());
                    pdf = pdfmerged;
                }
                proxy.getParameter("files.1.doc").setValue(pdfmerged);
                byte[] phoneAsBytes = StringUtils.EMPTY.getBytes();
                if (StringUtils.isNotEmpty(phoneInternational)) {
                    phoneAsBytes = phoneInternational.trim().replaceAll("\\s", "").replace("+", "00").getBytes();
                }
                proxy.getParameter("phone").setValue(phoneAsBytes);

            }
            redirect = this.getService().dictaoCall(proxy);
        } catch (final IOException e) {
            LOGGER.warn("Could not send the document to the REST server", e);
        }
        return redirect;
    }

    /**
     * Service de retour Dictao. Dans le cas d'une signature réalisée avec
     * succès, on met à jour le dossier et on sauvegarde le document signé.
     * Sinon, on annule la transaction
     *
     * @param status
     *            le status de retour
     * @param login
     *            le login de la personne ayant signé
     * @param offer
     *            l'offre en cours de signature
     * @param model
     *            model
     * @return string
     * @throws TechniqueException
     *             erreur technique exception
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     */
    @RequestMapping(value = "/public/dictao/callback", method = RequestMethod.GET)
    public String callbackDictao(@RequestParam("status") final CodeRetourDictaoEnum status, @RequestParam("login") final String login, final @RequestParam("offer") String offer, final Model model)
            throws TechnicalException {
        LOGGER.debug("Calling Stamper callback");

        final Map<String, String> mapOffer = new HashMap<>();

        mapOffer.put(ISignatureConstante.OFFER_TYPE, "cerfa.pdf");

        final String[] params = login.split(ISignatureConstante.MOTIF_SEP_LOGIN);
        if (null == mapOffer.get(offer) || params.length != 3) {
            throw new TechnicalException("Paramètres de retour Dictao invalides. Impossible de mettre à jour le document, merci de réessayer");
        }

        final String documentName = mapOffer.get(offer);
        final String txId = params[1];
        final String accessId = params[2];

        // TODO Create a private method that returns ProxyResult
        /*
         * Get proxy session from transaction id
         */
        final ProxySessionBean proxySession = this.proxySessionDataService.findSessionByTransactionId(txId);
        if (null == proxySession) {
            throw new TechnicalException("Cannot get a valid proxy session from Stamper");
        }

        /*
         * Init proxy result
         */
        final ProxyResultBean proxyResult = new ProxyResultBean();
        proxyResult.setStatus(status.getCode());
        proxyResult.setMessage(status.getDescription());
        LOGGER.info("Init proxy result with status '{}' with description '{}' ", status.getCode(), status.getDescription());

        // TODO : gestion des codes de retour

        /*
         * Check Dictao signature status
         */
        if (CodeRetourDictaoEnum.OK == status) {

            final byte[] signedDoc = this.getService().getDocument(txId, documentName);
            final Map<String, byte[]> documents = new HashMap<>();
            documents.put(IProxyConstante.PROXY_SIGNED_DOCUMENT, signedDoc);
            proxyResult.setDocuments(documents);

            this.getService().deleteAndFinishUser(login, accessId);
            this.getService().finishAndArchive(txId, proxySession, offer);

        } else if (CodeRetourDictaoEnum.CANCEL != status) {
            LOGGER.warn("Code retour catché dictao " + status.getCode() + "  : " + status.getDescription());
            this.getService().deleteAndFinishUser(login, accessId);
            this.getService().cancelTransaction(txId);
            proxyResult.setDocuments(null);
        } else if (CodeRetourDictaoEnum.CANCEL == status) {
            final String uiCallback = proxySession.getParameterAsString(ProxySessionParameterBean.URL_REDIRECT_PREVIOUS_STEP_UI);
            LOGGER.debug("Redirect to previous step with url : {}", uiCallback);
            return "redirect:" + uiCallback;
        }

        /*
         * Send proxy result to Nash WS
         */
        LOGGER.debug("Send proxy result to Nash WS");
        final String serviceCallback = proxySession.getParameterAsString(ProxySessionParameterBean.URL_CALLBACK_SERVICE);
        final Response serviceResponse = ClientBuilder.newClient().register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class).target(serviceCallback) //
                .request() //
                .accept(MediaType.APPLICATION_JSON).post(Entity.entity(proxyResult, MediaType.APPLICATION_JSON));

        LOGGER.debug("Calling Nash WS with status response {}", serviceResponse.getStatus());
        /*
         * Redirect to Nash UI
         */
        final String uiCallback = proxySession.getParameterAsString(ProxySessionParameterBean.URL_CALLBACK_UI);
        LOGGER.debug("Redirect to Nash UI with url : {}", uiCallback);
        return "redirect:" + uiCallback;

    }
}
