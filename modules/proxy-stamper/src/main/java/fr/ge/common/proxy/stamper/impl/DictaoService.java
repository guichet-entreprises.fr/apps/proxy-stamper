package fr.ge.common.proxy.stamper.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dictao.wsdl.dtp.frontoffice.v5.DocumentPort;
import com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException;
import com.dictao.wsdl.dtp.frontoffice.v5.TransactionPort;
import com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException;
import com.dictao.wsdl.dtp_wcs.v1.ContentPort;
import com.dictao.wsdl.dtp_wcs.v1.CreateContentRequest;
import com.dictao.wsdl.dtp_wcs.v1.CreateUserSpaceRequest;
import com.dictao.wsdl.dtp_wcs.v1.DeleteUserSpaceRequest;
import com.dictao.wsdl.dtp_wcs.v1.UserSpacePort;
import com.dictao.xsd.dtp.common.v5.ArchiveMetadata;
import com.dictao.xsd.dtp.common.v5.AuthenticationInfo;
import com.dictao.xsd.dtp.common.v5.Document;
import com.dictao.xsd.dtp.common.v5.Metadata;
import com.dictao.xsd.dtp.common.v5.PersonalInfo;
import com.dictao.xsd.dtp.common.v5.SignatureField;
import com.dictao.xsd.dtp.common.v5.SignatureInfo;
import com.dictao.xsd.dtp.common.v5.SignatureLayout;
import com.dictao.xsd.dtp.common.v5.SignatureLocation;
import com.dictao.xsd.dtp.common.v5.SignatureRendering;
import com.dictao.xsd.dtp.common.v5.SignatureRequest;
import com.dictao.xsd.dtp.common.v5.UserDN;
import com.dictao.xsd.dtp_wcs.common.v1.Contact;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;

import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.stamper.bean.StamperConfigurationBean;
import fr.ge.common.proxy.stamper.core.pdf.ZoneFieldElement;

/**
 * Class DictaoService.
 */
public class DictaoService {

    /** La constante LOGGER_FONC. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(DictaoService.class);

    /** Le type de document à signer. */
    public static final String DOCUMENT_TYPE = "CONTRACT";

    /** Le bean de configuration. */
    @Autowired
    private StamperConfigurationBean configuration;

    /** Le proxy de connexion au service transaction. */
    @Autowired
    private TransactionPort transactionPort;

    /** Le proxy de connexion au service document. */
    @Autowired
    private DocumentPort documentPort;

    /** user space port. */
    @Autowired
    private UserSpacePort userSpacePort;

    /** content port. */
    @Autowired
    private ContentPort contentPort;

    /** Le proxy de connexion au service transaction Bouchon. */
    @Autowired
    private TransactionPort transactionPortBouchon;

    /** Le proxy de connexion au service document Bouchon. */
    @Autowired
    private DocumentPort documentPortBouchon;

    /** user space port bouchon. */
    @Autowired
    private UserSpacePort userSpacePortBouchon;

    /** content port bouchon. */
    @Autowired
    private ContentPort contentPortBouchon;

    /** conversion . pdf to INCH **/
    private static final float POINTS_PER_INCH = 72;

    /** conversion inch to CM **/
    private static final float CM_PER_INCH = 25.4f / POINTS_PER_INCH;

    /**
     * Crée une transaction DICTAO.
     *
     * @return l'id de la transaction
     */
    public String createTransaction() {
        try {
            String value = getTransactionPortSE().createTransaction("guichet_entreprises", "scnge", "default", "default", null);
            return value;
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Ajoute un document à signer à la transaction.
     *
     * @param transactionId
     *            la transaction à utiliser
     * @param fileName
     *            file name
     * @param numeroDossier
     *            numero dossier
     * @param pdfToSign
     *            le document à signer
     * @param absolutPath
     *            absolut path
     */
    public void putDocument(final String transactionId, final String fileName, final String numeroDossier, final byte[] fileContent, final String zoneIdentifier) {
        Document newDocument = null;
        try {
            newDocument = new Document();
            newDocument.setFilename(fileName);
            newDocument.setMimetype("application/pdf");
            newDocument.setContent(fileContent);
            newDocument.setLabel("Formalité NASH");
            newDocument.setDescription(numeroDossier);

            /*
             * Définition des zones de signature à créer (seulement si DTP doit
             * ajouter les zones)
             */
            // (int page, int x, int y, int width, int height)
            if (null != this.getZonesPdfField(zoneIdentifier, fileContent)) {
                List<ZoneFieldElement> zonesPdfField = this.getZonesPdfField(zoneIdentifier, fileContent);
                for (ZoneFieldElement zoneFieldElement : zonesPdfField) {
                    newDocument.getSignatureField().addAll(buildSignatures(zoneFieldElement));
                }
            }

            /*
             * newDocument.getSignatureField().addAll(buildSignatures(new
             * ZoneFieldElement(1, 100, 100, 100, 100)));
             */
        } catch (FileNotFoundException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (IOException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }

        try {
            getDocumentPortSE().putDocument(transactionId, newDocument, DOCUMENT_TYPE);
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    private List<ZoneFieldElement> getZonesPdfField(final String zoneIdentifier, final byte[] fileContent) throws IOException {
        List<ZoneFieldElement> zoneFielElements = new ArrayList<ZoneFieldElement>();
        PdfReader reader = new PdfReader(fileContent);
        try {
            AcroFields fields = reader.getAcroFields();
            List<AcroFields.FieldPosition> positions = fields.getFieldPositions(zoneIdentifier);
            if (null != positions && !positions.isEmpty()) {
                for (int i = 0; i < positions.size(); i++) {
                    Rectangle rect = positions.get(i).position; // In points:
                    float x = rect.getLeft();
                    float y = rect.getBottom();
                    float width = rect.getWidth();
                    float height = rect.getHeight();
                    int page = positions.get(i).page;
                    ZoneFieldElement zonefiel = new ZoneFieldElement(page, x * CM_PER_INCH, y * CM_PER_INCH, width * CM_PER_INCH, height * CM_PER_INCH);
                    zoneFielElements.add(zonefiel);
                }
            }
        } finally {
            reader.close();
        }
        return zoneFielElements;
    }

    /**
     * Sign server transaction.
     *
     * @param txId
     *            tx id
     * @param utilisateur
     *            utilisateur
     */
    public void signServerTransaction(final String txId, final String nom, final String prenom, final String zoneId) {
        /* Définition des paramètres de signature */
        SignatureRequest signatureRequest = new SignatureRequest();

        /* Type du document à signer */
        signatureRequest.setType(DOCUMENT_TYPE);
        /* Nom de la zone à signer */
        signatureRequest.setLabel(zoneId);
        /* Paramètres de construction du visuel de signature */
        SignatureRendering rendering = new SignatureRendering();
        SignatureLayout signatureLayout = new SignatureLayout();

        /*
         * Nom du layout à appliquer (paramétré en configuration fonctionnelle
         * de DTP)
         */
        String layoutName = this.constructLayoutByNameSize(nom, prenom);
        signatureLayout.setName(layoutName);
        rendering.setLayout(signatureLayout);
        signatureRequest.setRendering(rendering);

        /* Signature cachet-serveur d’une zone */
        try {
            getDocumentPortSE().signDocuments(txId, Arrays.asList(signatureRequest));
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    /**
     * Ajout du layout selon la taille du nom et du prenom.
     *
     * @param nom
     *            nom
     * @param prenom
     *            prenom
     * @return string
     */
    public String constructLayoutByNameSize(final String nom, final String prenom) {

        if (StringUtils.isBlank(nom) || StringUtils.isBlank(prenom)) {
            LOGGER_FONC.error("L'utilisateur connecte ne posse pas de nom ou de prenom il n'est pas possible de déterminer le layout à utiliser.");
            return null;
        }

        /*
         * Nom du layout à appliquer (paramétré en configuration fonctionnelle
         * de DTP)
         */
        String layoutName = null;

        // Taille de l'ensemmble nom+prenom
        Integer tailleNomPrenom = StringUtils.length(nom + prenom);

        if (tailleNomPrenom > 28) {
            // layoutHandwritingS if N > 28
            layoutName = "layoutPersonalRight300x1500_41b";
        }
        if (tailleNomPrenom > 15 && tailleNomPrenom < 28) {
            // layoutHandwritingM if 14 < N < 28
            layoutName = "layoutPersonalBottom300x900_41b";
        } else {
            // layoutHandwritingL if N < 15
            layoutName = "layoutPersonalBottom400x850_60b";
        }
        LOGGER_FONC.info("Le layout utilisé pour la signature électronique de " + nom + " " + prenom + " est " + layoutName);

        return layoutName;
    }

    /**
     * Creates the user space.
     *
     * @param attributes
     *            attributes
     * @param login
     *            login
     * @param utilisateurBean
     *            utilisateur bean
     * @throws UnsupportedEncodingException
     *             unsupported encoding exception
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     */
    public void createUserSpace(final String attributes, final String login, final String civility, final String nom, final String prenom, final String email)
            throws UnsupportedEncodingException, IOException {
        // --- creation d'un espace utilisateur
        CreateUserSpaceRequest createUserSpaceRequest = new CreateUserSpaceRequest();
        // Le tenant
        createUserSpaceRequest.setTenant(getConfiguration().getDictaoTenantName());

        createUserSpaceRequest.setLogin(login);

        // Le nom de l'utilisateur final qui sera affiché dans l’IHM
        Contact contact = new Contact();
        contact.setFullname(this.civiliteSignature(civility) + " " + prenom + " " + nom);
        contact.setMail(email);
        createUserSpaceRequest.setContact(contact);
        createUserSpaceRequest.setAttributes(attributes);

        try {
            getUserSpacePortSE().create(createUserSpaceRequest);
        } catch (com.dictao.wsdl.dtp_wcs.v1.UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (com.dictao.wsdl.dtp_wcs.v1.SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    /**
     * Crée un accès pour un utilisateur.
     *
     * @param txId
     *            l'id de transaction
     * @param utilisateurBean
     *            utilisateur bean
     * @return un accessId
     */
    public String createUserAccess(final String txId, final String civility, final String lastName, final String firstName, final String phone) {
        PersonalInfo pi = new PersonalInfo();
        pi.setUserId(UUID.randomUUID().toString());
        /* Informations sur le signataire */
        SignatureInfo si = new SignatureInfo();
        si.setTitle(this.civiliteSignature(civility));
        si.setFirstName(firstName);
        si.setLastName(lastName);

        UserDN udn = new UserDN();
        udn.setCountryName("FR");
        udn.setOrganizationName("GUICHET_ENTREPRISES");
        udn.setOrganizationalUnitName("0002 404833048");
        udn.setCommonName(this.civiliteSignature(civility) + " " + firstName + " " + lastName);
        si.setUserDN(udn);
        pi.setSignatureInfo(si);

        /* Informations sur l’authentification */
        /*
         * Un seul moyen peut être mis en oeuvre dans un même user access : SMS
         * ou MAIL
         */
        AuthenticationInfo ai = new AuthenticationInfo();
        /*
         * La fourniture d'un numéro de téléphone détermine le mode de l'OTP :
         * SMS -- AU FORMAT INTERNATIONAL
         */
        LOGGER_FONC.info("Le numéro de téléphone utilisé pour la signature électronique DICTAO est le suivant " + phone);

        ai.setPhoneNumber(phone);
        /* Pour utiliser un OTP mail, il faut spécifier une adresse email : */
        // ai.setEmail(utilisateurBean.getEmail());
        pi.setAuthenticationInfo(ai);
        /* Liste des documents accessibles au signataire */

        List<String> authorizedDocs = Arrays.asList(new String[] { DOCUMENT_TYPE });
        /* Creation de l’accés utilisateur */
        Long timeout = 3600000L;
        Metadata metadata = null;

        /* l’accès utilisateur ne possède pas de metadonnées */
        try {
            return getTransactionPortSE().createUserAccess(txId, pi, timeout, metadata, authorizedDocs, "SIGNER");
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Reformatage de l'affichage pour la signature.
     *
     * @param civilite
     *            civilite
     * @return string
     */
    private String civiliteSignature(String civilite) {
        if (StringUtils.equals(civilite, "Monsieur")) {
            return "M.";
        } else {
            return "Mme";
        }
    }

    /**
     * Creates the content.
     *
     * @param login
     *            login
     * @param userAccessId
     *            user access id
     * @param attributes
     *            attributes
     * @param offerType
     *            offer type
     * @throws UnsupportedEncodingException
     *             unsupported encoding exception
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     */
    public void createContent(final String login, final String userAccessId, final String attributes, String offerType) throws UnsupportedEncodingException, IOException {
        // Ajout d'un contenu/offre dans un espace client.
        CreateContentRequest createContentRequest = new CreateContentRequest();
        // Le tenant.
        createContentRequest.setTenant(getConfiguration().getDictaoTenantName());
        // Le login. Doit être identique au login utilisé lors de la création de
        // l'espace utilisateur.
        createContentRequest.setLogin(login);
        // L'identifiant de l'offre.
        createContentRequest.setId(offerType);
        createContentRequest.setAttributes(attributes);
        try {
            getContentPortSE().create(createContentRequest);
        } catch (com.dictao.wsdl.dtp_wcs.v1.UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (com.dictao.wsdl.dtp_wcs.v1.SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    /**
     * Delete and finish user.
     *
     * @param login
     *            login
     * @param accessId
     *            access id
     */
    public void deleteAndFinishUser(final String login, final String accessId) {
        LOGGER_FONC.info("Calling Dictao delete and finish user space service");
        DeleteUserSpaceRequest req = new DeleteUserSpaceRequest();
        req.setLogin(login);
        req.setTenant(getConfiguration().getDictaoTenantName());
        try {
            getUserSpacePortSE().delete(req);
            getTransactionPortSE().finishUserAccess(accessId);
        } catch (com.dictao.wsdl.dtp_wcs.v1.UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (com.dictao.wsdl.dtp_wcs.v1.SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    /**
     * Accesseur sur l'attribut document.
     *
     * @param txId
     *            tx id
     * @param documentName
     *            document name
     * @return document
     */
    public Document getDocument(final String txId, final String documentName) {
        try {
            return getDocumentPortSE().getDocument(txId, documentName);
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Finish and archive.
     *
     * @param txId
     *            tx id
     * @param numeroDossier
     *            numero dossier
     * @param utilisateurBean
     *            utilisateur bean
     * @param typeSignature
     *            type signature
     */
    public void finishAndArchive(final String txId, final ProxySessionBean proxySession, final String typeSignature) {
        LOGGER_FONC.info("Calling Dictao finish and archive service");
        try {
            getTransactionPortSE().finishTransaction(txId);
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }

        try {
            final String lastName = proxySession.getParameterAsString("lastName");
            final String firstName = proxySession.getParameterAsString("firstName");
            final String email = proxySession.getParameterAsString("email");
            getTransactionPortSE().archiveTransaction(txId, null, buildArchiveMetada(txId, proxySession.getToken(), lastName, firstName, email, typeSignature));
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    /**
     * Cancel transaction.
     *
     * @param txId
     *            tx id
     */
    public void cancelTransaction(final String txId) {
        LOGGER_FONC.info("Calling Dictao cancel transaction service");
        try {
            getTransactionPortSE().cancelTransaction(txId);
        } catch (UserFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SystemFaultException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    /**
     * Builds the signatures.
     *
     * @param zoneFieldElement
     *            zone field element
     * @return list
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     */
    /* Définition des zones de signature à ajouter dans le contrat */
    private List<SignatureField> buildSignatures(ZoneFieldElement zoneFieldElement) throws IOException {
        List<SignatureField> signatureFields = new ArrayList<SignatureField>();

        /* Ajout d’une signature entité */
        SignatureField entitySignatureField = new SignatureField();
        entitySignatureField.setLabel("GE");
        entitySignatureField.setLocation(null);
        signatureFields.add(entitySignatureField);

        /* Ajout d’une signature personnelle */
        SignatureField personalSignatureField = new SignatureField();
        personalSignatureField.setLabel("contractor_field");
        int ratio = (int) (zoneFieldElement.getLargeur() / zoneFieldElement.getHauteur());
        LOGGER_FONC.info("Affichage du ratio pour le layout : " + ratio);
        // (int page, int x, int y, int width, int height)
        personalSignatureField.setLocation(buildVisibleSignature(zoneFieldElement.getPage(), (int) zoneFieldElement.getAbscisse(), (int) zoneFieldElement.getOrdonne(),
                (int) zoneFieldElement.getLargeur(), (int) zoneFieldElement.getHauteur()));
        signatureFields.add(personalSignatureField);
        return signatureFields;
    }

    /**
     * Builds the visible signature.
     *
     * @param page
     *            page
     * @param x
     *            x
     * @param y
     *            y
     * @param width
     *            width
     * @param height
     *            height
     * @return signature location
     */
    private SignatureLocation buildVisibleSignature(final int page, final int x, final int y, final int width, final int height) {
        SignatureLocation signatureLocation = new SignatureLocation();
        signatureLocation.setPage(page);
        signatureLocation.setX(x);
        signatureLocation.setY(y);
        signatureLocation.setWidth(width);
        signatureLocation.setHeight(height);
        return signatureLocation;
    }

    /**
     * Builds the archive metada.
     *
     * @param txId
     *            tx id
     * @param numeroDossier
     *            numero dossier
     * @param utilisateurBean
     *            utilisateur bean
     * @param typeSignature
     *            type signature
     * @return archive metadata
     */
    private ArchiveMetadata buildArchiveMetada(final String txId, final String numeroDossier, final String nom, final String prenom, final String email, final String typeSignature) {
        ArchiveMetadata archiveMD = new ArchiveMetadata();
        // The following metadata are mandatory
        ArchiveMetadata.StringMetadata fileNameMD = new ArchiveMetadata.StringMetadata();
        fileNameMD.setId("fileName");
        fileNameMD.setValue(txId + ".zip");
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(fileNameMD);

        ArchiveMetadata.StringMetadata transactionIdMD = new ArchiveMetadata.StringMetadata();
        transactionIdMD.setId("transactionId");
        transactionIdMD.setValue(txId);
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(transactionIdMD);

        ArchiveMetadata.StringMetadata tenantMD = new ArchiveMetadata.StringMetadata();
        tenantMD.setId("tenant");
        tenantMD.setValue(getConfiguration().getDictaoTenantName());
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(tenantMD);

        // The following metadata are optional
        ArchiveMetadata.DateMetadata transactionDateMD = new ArchiveMetadata.DateMetadata();
        transactionDateMD.setId("businessDate");
        GregorianCalendar archiveCal = new GregorianCalendar();
        archiveCal.setTime(new Date());

        try {
            transactionDateMD.setValue(DatatypeFactory.newInstance().newXMLGregorianCalendar(archiveCal));
        } catch (DatatypeConfigurationException ex) {
            LOGGER_FONC.info("> An user exception occured: unable to set transactionDate");
            LOGGER_FONC.info("> Message : '{0}'", ex.getMessage());
        }

        ArchiveMetadata.StringMetadata businessTag1 = new ArchiveMetadata.StringMetadata();
        businessTag1.setId("businessTag1");
        businessTag1.setValue(numeroDossier);
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(businessTag1);

        ArchiveMetadata.StringMetadata businessTag2 = new ArchiveMetadata.StringMetadata();
        businessTag2.setId("businessTag2");
        businessTag2.setValue(email);
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(businessTag2);

        ArchiveMetadata.StringMetadata businessTag3 = new ArchiveMetadata.StringMetadata();
        businessTag3.setId("businessTag3");
        businessTag3.setValue(typeSignature);
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(businessTag3);

        ArchiveMetadata.StringMetadata firstName1 = new ArchiveMetadata.StringMetadata();
        firstName1.setId("firstName1");
        firstName1.setValue(prenom);
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(firstName1);

        ArchiveMetadata.StringMetadata lastName1 = new ArchiveMetadata.StringMetadata();
        lastName1.setId("lastName1");
        lastName1.setValue(nom);
        archiveMD.getStringMetadataOrDateMetadataOrLongMetadata().add(lastName1);

        return archiveMD;
    }

    /**
     * Accesseur sur l'attribut {@link #configuration}.
     *
     * @return the configuration
     */
    public StamperConfigurationBean getConfiguration() {
        return configuration;
    }

    /**
     * Mutateur sur l'attribut {@link #configuration}.
     *
     * @param configuration
     *            the configuration to set
     */
    public void setConfiguration(StamperConfigurationBean configuration) {
        this.configuration = configuration;
    }

    /**
     * Accesseur sur l'attribut {@link #transaction port}.
     *
     * @return the transactionPort
     */
    public TransactionPort getTransactionPort() {
        return transactionPort;
    }

    /**
     * Mutateur sur l'attribut {@link #transaction port}.
     *
     * @param transactionPort
     *            the transactionPort to set
     */
    public void setTransactionPort(TransactionPort transactionPort) {
        this.transactionPort = transactionPort;
    }

    /**
     * Accesseur sur l'attribut {@link #document port}.
     *
     * @return the documentPort
     */
    public DocumentPort getDocumentPort() {
        return documentPort;
    }

    /**
     * Mutateur sur l'attribut {@link #document port}.
     *
     * @param documentPort
     *            the documentPort to set
     */
    public void setDocumentPort(DocumentPort documentPort) {
        this.documentPort = documentPort;
    }

    /**
     * Accesseur sur l'attribut {@link #user space port}.
     *
     * @return the userSpacePort
     */
    public UserSpacePort getUserSpacePort() {
        return userSpacePort;
    }

    /**
     * Mutateur sur l'attribut {@link #user space port}.
     *
     * @param userSpacePort
     *            the userSpacePort to set
     */
    public void setUserSpacePort(UserSpacePort userSpacePort) {
        this.userSpacePort = userSpacePort;
    }

    /**
     * Accesseur sur l'attribut {@link #content port}.
     *
     * @return the contentPort
     */
    public ContentPort getContentPort() {
        return contentPort;
    }

    /**
     * Mutateur sur l'attribut {@link #content port}.
     *
     * @param contentPort
     *            the contentPort to set
     */
    public void setContentPort(ContentPort contentPort) {
        this.contentPort = contentPort;
    }

    /**
     * Accesseur sur l'attribut {@link #transaction port bouchon}.
     *
     * @return the transactionPortBouchon
     */
    public TransactionPort getTransactionPortBouchon() {
        return transactionPortBouchon;
    }

    /**
     * Mutateur sur l'attribut {@link #transaction port bouchon}.
     *
     * @param transactionPortBouchon
     *            the transactionPortBouchon to set
     */
    public void setTransactionPortBouchon(TransactionPort transactionPortBouchon) {
        this.transactionPortBouchon = transactionPortBouchon;
    }

    /**
     * Accesseur sur l'attribut {@link #document port bouchon}.
     *
     * @return the documentPortBouchon
     */
    public DocumentPort getDocumentPortBouchon() {
        return documentPortBouchon;
    }

    /**
     * Mutateur sur l'attribut {@link #document port bouchon}.
     *
     * @param documentPortBouchon
     *            the documentPortBouchon to set
     */
    public void setDocumentPortBouchon(DocumentPort documentPortBouchon) {
        this.documentPortBouchon = documentPortBouchon;
    }

    /**
     * Accesseur sur l'attribut {@link #user space port bouchon}.
     *
     * @return the userSpacePortBouchon
     */
    public UserSpacePort getUserSpacePortBouchon() {
        return userSpacePortBouchon;
    }

    /**
     * Mutateur sur l'attribut {@link #user space port bouchon}.
     *
     * @param userSpacePortBouchon
     *            the userSpacePortBouchon to set
     */
    public void setUserSpacePortBouchon(UserSpacePort userSpacePortBouchon) {
        this.userSpacePortBouchon = userSpacePortBouchon;
    }

    /**
     * Accesseur sur l'attribut {@link #content port bouchon}.
     *
     * @return the contentPortBouchon
     */
    public ContentPort getContentPortBouchon() {
        return contentPortBouchon;
    }

    /**
     * Mutateur sur l'attribut {@link #content port bouchon}.
     *
     * @param contentPortBouchon
     *            the contentPortBouchon to set
     */
    public void setContentPortBouchon(ContentPort contentPortBouchon) {
        this.contentPortBouchon = contentPortBouchon;
    }

    /**
     * Retourne le bean TransactionPort en fonction de la feature Togglz.
     *
     * @return transaction port SE
     */
    public TransactionPort getTransactionPortSE() {
        return transactionPort;
    }

    /**
     * Retourne le bean DocumentPort en fonction de la feature Togglz.
     *
     * @return document port SE
     */
    public DocumentPort getDocumentPortSE() {
        return documentPort;
    }

    /**
     * Retourne le bean UserSpacePort en fonction de la feature Togglz.
     *
     * @return user space port SE
     */
    public UserSpacePort getUserSpacePortSE() {
        return userSpacePort;
    }

    /**
     * Retourne le bean UserSpacePort en fonction de la feature Togglz.
     *
     * @return content port SE
     */
    public ContentPort getContentPortSE() {
        return contentPort;
    }
}
