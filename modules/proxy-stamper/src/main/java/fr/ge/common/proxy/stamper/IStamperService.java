/**
 * 
 */
package fr.ge.common.proxy.stamper;

import fr.ge.common.proxy.data.bean.ProxySessionBean;

/**
 * @author bsadil
 *
 */
public interface IStamperService {

    /**
     * 
     * @return
     */
    String createTransaction();

    /**
     * @param session
     * @return
     */
    String dictaoCall(final ProxySessionBean session);

    /**
     * @param txId
     * @param documentName
     * @return
     */
    byte[] getDocument(String txId, String documentName);

    /**
     * @param login
     * @param accessId
     */
    void deleteAndFinishUser(String login, String accessId);

    /**
     * @param txId
     * @param proxySession
     * @param offer
     */
    void finishAndArchive(String txId, ProxySessionBean proxySession, String offer);

    /**
     * @param txId
     */
    void cancelTransaction(String txId);
}
