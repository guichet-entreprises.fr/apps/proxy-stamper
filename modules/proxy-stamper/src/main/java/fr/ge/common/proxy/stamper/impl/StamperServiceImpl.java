/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.proxy.stamper.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.data.service.IProxySessionDataService;
import fr.ge.common.proxy.stamper.IStamperService;
import fr.ge.common.proxy.stamper.bean.StamperConfigurationBean;
import fr.ge.common.proxy.stamper.constante.ISignatureConstante;

/**
 * Stamper proxy implementation.
 *
 * @author aolubi
 */
public class StamperServiceImpl implements IStamperService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StamperServiceImpl.class);

    /** Le service dictao. */
    @Autowired
    private DictaoService dictaoService;

    /** Le service dictao. */
    @Autowired
    private StamperConfigurationBean stamperConfigurationBean;

    @Autowired
    private IProxySessionDataService proxySessionDataService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String dictaoCall(final ProxySessionBean session) {

        LOGGER.debug("Get proxy session parameters to redirect user to DICTAO service");
        String phone = null;
        // try {
        phone = session.getParameterAsString("phone");
        final String civility = session.getParameterAsString("civility");
        final String lastName = session.getParameterAsString("lastName");
        final String firstName = session.getParameterAsString("firstName");
        final String email = session.getParameterAsString("email");
        final byte[] docContent = session.getParameterAsBytes("files.1.doc");
        final String zoneId = session.getParameterAsString("files.1.zoneId");

        final String txId = this.createTransaction();
        LOGGER.debug("Creating Dictao transaction");

        /*
         * Link the Dictao transaction identifier with the proxy session
         */
        this.proxySessionDataService.addTransaction(session.getToken(), txId);

        // // TODO Remove Use byte[] insteadof InputStream and absolute path
        this.dictaoService.putDocument(txId, "cerfa.pdf", session.getToken(), docContent, zoneId);
        this.dictaoService.signServerTransaction(txId, lastName, firstName, zoneId);
        LOGGER.debug("Signing Dictao transaction");
        final String accessId = this.dictaoService.createUserAccess(txId, civility, lastName, firstName, phone);

        final String userspaceAttr = this.generateUserSpaceConfiguration();
        final String login = "login" + ISignatureConstante.MOTIF_SEP_LOGIN + txId + ISignatureConstante.MOTIF_SEP_LOGIN + accessId;
        try {
            this.dictaoService.createUserSpace(userspaceAttr, login, civility, lastName, firstName, email);
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        final String contentAttr = this.generateContentConfiguration(accessId, lastName, firstName, phone);
        try {
            this.dictaoService.createContent(login, accessId, contentAttr, ISignatureConstante.OFFER_TYPE);
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        final String redirectUrl = this.getUrlCallback(login, ISignatureConstante.OFFER_TYPE);
        LOGGER.info(" - " + redirectUrl + " for redirect mode");

        LOGGER.debug("Redirect to Dictao interface");
        return "redirect:" + redirectUrl;
    }

    /**
     * Generate user space configuration.
     *
     * @return string
     */
    private String generateUserSpaceConfiguration() {
        final JSONObject jsonConf = new JSONObject();
        jsonConf.put("label", "Signature de votre dossier");
        jsonConf.put("showProgress", false);
        jsonConf.put("todoListLabel", "Statut de la signature de votre dossier");
        return jsonConf.toString();

    }

    /**
     * Generate content configuration.
     *
     * @param accessId
     *            access id
     * @param telephone
     *            phone number
     * @return string
     */
    private String generateContentConfiguration(final String accessId, final String nom, final String prenom, final String telephone) {
        final JSONObject jsonConf = new JSONObject();
        jsonConf.put("type", "signatureRequest");
        jsonConf.put("processCompleted", false);
        jsonConf.put("access_id", accessId);
        jsonConf.put("todo", "Signer le document");
        jsonConf.put("label", "Modification/Cessation d'entreprise");
        jsonConf.put("description",
                "<h3>Modification/Cessation</h3><p style='color:blue'>Vous avez fait une demande de " + "modification ou de cessation de votre entreprise.</p><p>Afin de finaliser votre demande, nous "
                        + "vous invitons &agrave; vérifier les informations du formulaire suivant, puis &agrave; "
                        + "signifier votre accord au moyen d'une signature &eacute;lectronique. Pour cela, il vous "
                        + "suffit de cliquer sur le bouton &#171; Signer le document &#187; ci-dessous (le clic sur ce "
                        + "premier bouton n'entrainant aucun engagement de votre part).</p><p>La signature de ce " + "document finalisera votre demande.</p>");

        final JSONObject convention = new JSONObject();
        convention.put("enabled", true);
        convention.put("label", "Convention de Preuve &Eacute;lectronique");
        convention.put("description",
                "J’accepte que les documents établis et produits par voie électronique ou leur reproduction sur " + "un support informatique constituent la preuve de dépôt de mon dossier.");
        convention.put("clause", "J'ai pris connaissance de la mention ci-dessus et l'accepte sans r&eacute;serve");
        convention.put("button_prev", "Retour");
        convention.put("button_next", "Continuer");
        jsonConf.put("convention", convention);

        final JSONObject overview = new JSONObject();
        overview.put("before", false);
        overview.put("after", false);
        jsonConf.put("overview", overview);

        jsonConf.put("prompt", "Liste des documents à lire pour signer.<br/>Sélectionner-les dans la liste ci-dessous pour les consulter.");
        jsonConf.put("howto", "Vous devez consulter tous les documents marqu&eacute; comme <b>&agrave; lire</b> avant de pouvoir signer le dossier.");

        final JSONObject buttonLabel = new JSONObject();
        buttonLabel.put("SignOffer", "SIGNER LE DOCUMENT");
        buttonLabel.put("OfferPending", "REPRENDRE LA SIGNATURE");
        buttonLabel.put("OfferSigned", "DOCUMENT SIGNE");
        buttonLabel.put("exitHome", "RETOUR");
        buttonLabel.put("exitRedirect", "TERMINER");
        jsonConf.put("buttonLabel", buttonLabel);

        final JSONObject footer = new JSONObject();
        footer.put("DocLeftToRead", "document(s) restant à lire pour pouvoir signer le dossier");
        footer.put("DocReadingDone", "Vous avez lu tous les documents requis.");
        footer.put("SignatureProcessCompleted", "La procédure est terminée.");
        jsonConf.put("footer", footer);

        final JSONObject documentviewer = new JSONObject();
        final JSONObject nav = new JSONObject();
        nav.put("label", "Liste des documents");
        nav.put("header", "Liste des documents &agrave; lire pour signer le dossier");
        nav.put("footer", "<b>SUIVEZ LES ETAPES CI-CONTRE</b><br/>POUR SIGNER VOTRE DOSSIER");
        documentviewer.put("nav", nav);

        documentviewer.put("link_to_top", "Haut du document");
        documentviewer.put("header_read_all_prompt", "Parcourez le document dans son int&eacute;gralit&eacute; pour le marquer comme lu");
        documentviewer.put("footer_doc_read", "Le document est maintenant marqu&eacute; comme lu.");
        documentviewer.put("footer_all_doc_read", "Vous avez lu tous les documents requis, <b>vous pouvez signer le dossier</b>.");

        final JSONObject sigDetails = new JSONObject();
        sigDetails.put("description_signed", "Cliquez ici pour voir le détail de la signature");
        sigDetails.put("description_unsigned", "[zone non signée]");
        sigDetails.put("valid_signature", "La signature est valide");
        sigDetails.put("invalid_signature", "La signature est invalide");
        sigDetails.put("text_signature", "Détails de la signature");
        sigDetails.put("informations_signer", "Informations sur le signataire : ");
        sigDetails.put("signer", "Signataire : ");
        sigDetails.put("date_signature", "Date de la signature : ");
        sigDetails.put("additional_informations", "Informations compl&eacute;mentaires : ");
        sigDetails.put("timestamp_date", "La signature est munie d'un jeton d'horodatage : ");
        sigDetails.put("timestamp_invalid", "La signature est munie d'un jeton d'horodatage invalide");
        sigDetails.put("timestamp_missing", "La signature n'est pas munie de jeton d'horodatage");
        documentviewer.put("sig_details", sigDetails);
        documentviewer.put("button_prev", "Retour");
        documentviewer.put("button_next", "Signer le document");
        jsonConf.put("documentviewer", documentviewer);

        final JSONArray documents = new JSONArray();
        final JSONObject docCerfa = new JSONObject();
        docCerfa.put("id", "contract");
        docCerfa.put("type", DictaoService.DOCUMENT_TYPE);
        docCerfa.put("label", "Formulaire à signer");
        docCerfa.put("description", "Formulaire");
        docCerfa.put("requirement", "read");
        // -->Récupération du bean GentUserBean correspondant à l'utilisateur
        // courant
        final String layoutPersonal = this.dictaoService.constructLayoutByNameSize(nom, prenom);
        docCerfa.put("signature_fields", new JSONArray("[{label:\"contractor_field\",layout:\"" + layoutPersonal + "\"}]"));
        documents.put(docCerfa);
        jsonConf.put("documents", documents);

        final JSONObject consent = new JSONObject();
        consent.put("clauses", new JSONArray());
        consent.put("cert", new JSONObject("{enabled:false}"));
        consent.put("sign", new JSONObject("{enabled:false}"));

        final JSONObject otp = new JSONObject();
        otp.put("enabled", true);
        otp.put("title", "Consentement de l'utilisateur");
        otp.put("description", "Vous venez de reçevoir un code par SMS au numéro : " + telephone);
        otp.put("label_input", "Entrez le code ici :");
        otp.put("label_send", "Renvoyer le SMS");
        otp.put("error_otp_invalid",
                "Le code que vous avez renseign&eacute; est incorrect.<br />Veuillez v&eacute;rifier le code " + "qui vous a &eacute;t&eacute; transmis par SMS et ressaisissez-le.");
        otp.put("error_otp_expired", "Le d&eacute;lai de validit&eacute; de votre code a expir&eacute;.<br />Vous devez " + "renouveler votre demande de signature &eacute;lectronique.");
        otp.put("error_otp_blocked", "Suite &agrave; de nombreuses erreurs de saisies de votre part, votre num&eacute;ro de t&eacute;l&eacute;phone " + telephone
                + " ne vous permet plus de signer &eacute;lectroniquement votre d&eacute;marche.<br />" + "Nous vous invitons &agrave; renseigner un nouveau num&eacute;ro de t&eacute;l&eacute;phone "
                + "dans votre compte utilisateur ou &agrave; contacter le Guichet " + "Entreprises pour d&eacute;bloquer votre num&eacute;ro de t&eacute;l&eacute;phone actuel.");
        otp.put("button_prev", "Retour");
        otp.put("button_next", "Terminer");
        consent.put("otp", otp);

        jsonConf.put("consent", consent);
        return jsonConf.toString();

    }

    /**
     * Génère l'url de callback.
     *
     * @param login
     *            le login généré
     * @param offerType
     *            le type de signature demandée
     * @return l'url de retour générée
     */
    private String getUrlCallback(final String login, final String offerType) {
        StringBuilder sb = new StringBuilder(this.stamperConfigurationBean.getDictaoEndUserUrl());
        sb.append("offre/");
        sb.append(offerType);
        sb.append("/convention?login=");
        sb.append(login);
        sb.append("&url=");
        try {
            sb.append(URLEncoder.encode(this.stamperConfigurationBean.getUrlStamper() + "/public/dictao/callback?status={STATUS}&login={LOGIN}&offer={OFFER}", "UTF-8"));
        } catch (final UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return sb.toString();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] getDocument(String txId, String documentName) {
        return this.dictaoService.getDocument(txId, documentName).getContent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAndFinishUser(String login, String accessId) {
        this.dictaoService.deleteAndFinishUser(login, accessId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void finishAndArchive(String txId, ProxySessionBean proxySession, String offer) {
        this.dictaoService.finishAndArchive(txId, proxySession, offer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancelTransaction(String txId) {
        this.dictaoService.cancelTransaction(txId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createTransaction() {
        return this.dictaoService.createTransaction();
    }

}
