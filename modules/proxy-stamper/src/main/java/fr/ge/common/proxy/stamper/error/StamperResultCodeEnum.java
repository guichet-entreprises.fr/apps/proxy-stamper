/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.proxy.stamper.error;

import java.util.regex.Pattern;

import fr.ge.common.proxy.resultcode.IProxyResultCodeEnum;

/**
 * Stamper result codes.
 *
 * @author jpauchet
 */
public enum StamperResultCodeEnum implements IProxyResultCodeEnum {

    TODO(null, ".*", null, false);

    /** The code, if it is not an error category. */
    private String code;

    /** The code regex, if it is an error category. */
    private Pattern codeRegex;

    /** The message. */
    private String message;

    /** Display a message ?. */
    private boolean display;

    /**
     * Constructor.
     *
     * @param code
     *            the code
     * @param codeRegex
     *            the code regex
     * @param message
     *            the message
     * @param display
     *            display ?
     */
    StamperResultCodeEnum(final String code, final String codeRegex, final String message, final boolean display) {
        this.code = code;
        this.codeRegex = Pattern.compile(codeRegex);
        this.message = message;
        this.display = display;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCode() {
        return this.code;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pattern getCodeRegex() {
        return this.codeRegex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessage() {
        return this.message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplay() {
        return this.display;
    }
}
