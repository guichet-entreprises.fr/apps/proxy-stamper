package fr.ge.common.proxy.stamper.bean;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * Le Class StamperConfigurationBean.
 */
@Component
public class StamperConfigurationBean implements Serializable {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 9130236870053097709L;

    /** Le nom du tenant dictao. */
    private String dictaoTenantName;

    /** L'url de base sur laquelle rediriger l'utilisateur pour singature. */
    private String dictaoEndUserUrl;

    /** Chaine de vérification de la réponse du service echo. */
    private String dictaoCheckString;

    /**
     * Indicatif téléphonique internationnal attendu par le service de signature
     * dictao.
     */
    private String indicatifTelephoniqueInternationnalDictao;

    /**
     * L'url BOUCHON de base sur laquelle rediriger l'utilisateur pour
     * singature.
     */
    private String dictaoEndUserUrlBouchon;

    /**
     * URL publique de la bulle stamper.
     */
    private String urlStamper;

    /** Instance statique du bean StamperConfigurationBean. */
    private static StamperConfigurationBean instance = new StamperConfigurationBean();

    /**
     * Get le tenant name dictao.
     * 
     * @return le tenant name dictao
     */
    public String getDictaoTenantName() {
        return dictaoTenantName;
    }

    /**
     * Set le tenant name dictao.
     * 
     * @param dictaoTenantName
     *            le nouveau tenant name dictao
     */
    public void setDictaoTenantName(final String dictaoTenantName) {
        this.dictaoTenantName = dictaoTenantName;
    }

    /**
     * Get l'url end user dictao.
     * 
     * @return l'url end user dictao
     */
    public String getDictaoEndUserUrl() {
        return dictaoEndUserUrl;
    }

    /**
     * Set l'url end user dictao.
     *
     * @param dictaoEndUserUrl
     *            l'url end user dictao
     */
    public void setDictaoEndUserUrl(final String dictaoEndUserUrl) {
        this.dictaoEndUserUrl = dictaoEndUserUrl;
    }

    /**
     * Retour le parametre dictaoCheckString.
     *
     * @return dictao check string
     */
    public String getDictaoCheckString() {
        return dictaoCheckString;
    }

    /**
     * Sette le parametre dictaoCheckString.
     *
     * @param dictaoCheckString
     *            la nouvelle valeur de l'attribut dictao check string
     */
    public void setDictaoCheckString(final String dictaoCheckString) {
        this.dictaoCheckString = dictaoCheckString;
    }

    /**
     * Accesseur sur l'attribut {@link #dictao end user url bouchon}.
     *
     * @return the dictaoEndUserUrlBouchon
     */
    public String getDictaoEndUserUrlBouchon() {
        return dictaoEndUserUrlBouchon;
    }

    /**
     * Mutateur sur l'attribut {@link #dictao end user url bouchon}.
     *
     * @param dictaoEndUserUrlBouchon
     *            the dictaoEndUserUrlBouchon to set
     */
    public void setDictaoEndUserUrlBouchon(final String dictaoEndUserUrlBouchon) {
        this.dictaoEndUserUrlBouchon = dictaoEndUserUrlBouchon;
    }

    /**
     * Récupère l'instance unique de GipgeConfigurationBean.
     *
     * @return unique instance de GipgeConfigurationBean
     */
    public static StamperConfigurationBean getInstance() {
        return instance;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #indicatifTelephoniqueInternationnalDictao}.
     *
     * @return String indicatifTelephoniqueInternationnalDictao
     */
    public String getIndicatifTelephoniqueInternationnalDictao() {
        return indicatifTelephoniqueInternationnalDictao;
    }

    /**
     * Mutateur sur l'attribut
     * {@link #indicatifTelephoniqueInternationnalDictao}.
     *
     * @param indicatifTelephoniqueInternationnalDictao
     *            la nouvelle valeur de l'attribut
     *            indicatifTelephoniqueInternationnalDictao
     */
    public void setIndicatifTelephoniqueInternationnalDictao(String indicatifTelephoniqueInternationnalDictao) {
        this.indicatifTelephoniqueInternationnalDictao = indicatifTelephoniqueInternationnalDictao;
    }

    /**
     * Accesseur sur l'attribut {@link #urlStamper}.
     *
     * @return String urlStamper
     */
    public String getUrlStamper() {
        return urlStamper;
    }

    /**
     * Mutateur sur l'attribut {@link #urlStamper}.
     *
     * @param urlStamper
     *            la nouvelle valeur de l'attribut urlStamper
     */
    public void setUrlStamper(String urlStamper) {
        this.urlStamper = urlStamper;
    }
}
