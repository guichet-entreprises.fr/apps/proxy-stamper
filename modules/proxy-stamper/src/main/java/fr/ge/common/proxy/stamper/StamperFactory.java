package fr.ge.common.proxy.stamper;

/**
 * Proxy factory.
 */
public interface StamperFactory {

    /**
     * Return the proxy service (live or mock).
     * 
     * @param mode
     *            the proxy mode
     * @return the proxy service
     */
    IStamperService get(final String mode);
}
