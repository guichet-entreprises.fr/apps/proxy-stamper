package fr.ge.common.proxy.stamper.pdf.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;

import fr.ge.common.utils.test.AbstractTest;

public class PdfDocumentImplTest extends AbstractTest {

    private PdfDocumentImpl service = new PdfDocumentImpl();

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        // Nothing to do
    }

    @Test
    public void testImage() throws UnsupportedEncodingException, IOException {
        assertTrue(this.service.image("image/jpg"));
    }

    @Test
    public void testAcceptExtensions() throws UnsupportedEncodingException, IOException {
        assertTrue(this.service.acceptExtensions("application/pdf"));
    }

    @Test
    public void testAppend() {
        byte[] docAsBytes = this.resourceAsBytes("document.pdf");
        byte[] specimenAsBytes = this.resourceAsBytes("document.pdf");
        assertNotNull(this.service.append(specimenAsBytes, docAsBytes, "application/pdf"));

        specimenAsBytes = this.resourceAsBytes("specimen.png");
        assertNotNull(this.service.append(specimenAsBytes, docAsBytes, "image/png"));
    }
}
