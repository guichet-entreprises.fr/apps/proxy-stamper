package fr.ge.common.proxy.stamper.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.ws.v1.bean.ProxyResultBean;
import fr.ge.common.proxy.controller.ProxyFactory;
import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.data.bean.ProxySessionParameterBean;
import fr.ge.common.proxy.data.service.IProxySessionDataService;
import fr.ge.common.proxy.stamper.constante.CodeRetourDictaoEnum;
import fr.ge.common.proxy.stamper.constante.ISignatureConstante;
import fr.ge.common.proxy.stamper.mock.StamperServiceMock;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml", "classpath:spring/rest-client-context.xml" })
@WebAppConfiguration
public class StamperControllerTest {

    /** The mvc. */
    private MockMvc mvc;

    /** The web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    @Autowired
    private ProxyFactory factory;

    @Autowired
    private StamperServiceMock dictaoService;

    @Autowired
    private IProxySessionDataService proxySessionDataServiceImpl;

    private String accessIdMock;

    private String txIdMock;

    private String login;

    protected static final String ENDPOINT = "http://localhost:8888/test";

    protected Server server;

    protected ITestService service;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.service = mock(ITestService.class);
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext) //
                .build();

        Random random = new Random();
        accessIdMock = String.valueOf(random.nextInt());
        txIdMock = String.valueOf(random.nextInt());
        login = txIdMock + "_" + accessIdMock + "_" + "varinutile";

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JacksonJaxbJsonProvider());
        serverFactory.setProviders(providers);

        this.server = serverFactory.create();
    }

    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    /**
     * Test callback Dicato signature.
     * 
     * @throws Exception
     */
    @Test
    public void testCallbackDictaoOK() throws Exception {

        ProxySessionBean proxySession = mock(ProxySessionBean.class);
        proxySession.setToken("1234");
        proxySession.setTransactionId(txIdMock);
        when(proxySession.getParameterAsString(ProxySessionParameterBean.URL_CALLBACK_SERVICE)).thenReturn(ENDPOINT + "/v1/Record/code/1/step/1/phase/pre/process/1");
        when(proxySession.getParameterAsString(ProxySessionParameterBean.URL_CALLBACK_UI)).thenReturn(ENDPOINT);
        when(this.proxySessionDataServiceImpl.findSessionByTransactionId(any(String.class))).thenReturn(proxySession);
        when(this.service.uploadProxyResult(any(), any(), any(), any(), any())).thenReturn(Response.ok(true).build());

        // Call callback signature
        this.mvc.perform( //
                get("/public/dictao/callback") //
                        .param("status", CodeRetourDictaoEnum.OK.getCode()) //
                        .param("login", login) //
                        .param("offer", ISignatureConstante.OFFER_TYPE) //
        ) //
                .andExpect(status().is3xxRedirection());
    }

    /**
     * Test download document.
     * 
     * @throws Exception
     */
    @Test
    public void testDownload() throws Exception {
        // prepare
        ProxySessionBean proxySession = mock(ProxySessionBean.class);
        proxySession.setToken("1234");
        proxySession.setTransactionId(txIdMock);
        when(this.proxySessionDataServiceImpl.findByToken(anyString())).thenReturn(proxySession);
        when(proxySession.getParameterAsBytes("files.1.doc")).thenReturn("Hello word".getBytes());

        // call
        this.mvc.perform(MockMvcRequestBuilders.get("/public/document/download/{token}", "1234") //
                .param("modal", "false")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_DISPOSITION, "attachement; filename=document.pdf")) //
                .andExpect(MockMvcResultMatchers.content().bytes("Hello word".getBytes()));
    }

    /**
     * Test upload.
     * 
     * @throws Exception
     */
    @Test
    public void testUpload() throws Exception {
        // prepare
        ProxySessionBean proxySession = mock(ProxySessionBean.class);
        proxySession.setToken("1234");
        proxySession.setTransactionId(txIdMock);
        when(this.proxySessionDataServiceImpl.findByToken(anyString())).thenReturn(proxySession);
        when(this.dictaoService.dictaoCall(any(ProxySessionBean.class))).thenReturn("redirect:" + ENDPOINT);

        ProxySessionParameterBean phoneParameter = mock(ProxySessionParameterBean.class);
        when(proxySession.getParameter("phone")).thenReturn(phoneParameter);
        when(proxySession.getParameter("files.1.doc")).thenReturn(phoneParameter);
        when(proxySession.getParameterAsBytes("files.1.doc")).thenReturn("Hello word".getBytes());

        final MultipartFile file = mock(MultipartFile.class);
        when(file.getContentType()).thenReturn("application/octet-stream");
        when(file.getBytes()).thenReturn("Hello word".getBytes());

        this.mvc.perform(MockMvcRequestBuilders.fileUpload("/public/document/upload") //
                .file(new MockMultipartFile("doc", "document.pdf", "application/octet-stream", "Hello word".getBytes())) //
                .param("token", "1234") //
                .param("phone.international", "+33 612345678")) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
    }

    /**
     * Test init.
     * 
     * @throws Exception
     */
    @Test
    public void testInit() throws Exception {
        // prepare
        ProxySessionBean proxySession = mock(ProxySessionBean.class);
        proxySession.setToken("1234");
        proxySession.setTransactionId(txIdMock);
        when(this.proxySessionDataServiceImpl.findByToken(anyString())).thenReturn(proxySession);
        when(proxySession.getParameterAsString("phone")).thenReturn("0123456789");
        when(proxySession.getParameterAsString("lastName")).thenReturn("lastName");
        when(proxySession.getParameterAsString("firstName")).thenReturn("firstName");
        when(proxySession.getParameterAsString("URL_REDIRECT_UI_FIRST_PAGE")).thenReturn("/");

        this.mvc.perform(MockMvcRequestBuilders.get("/public/document") //
                .param("token", "1234")) //
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Test callback Dictao with cancel signature.
     * 
     * @throws Exception
     */
    @Test
    public void testCallbackDictaoCancel() throws Exception {

        ProxySessionBean proxySession = mock(ProxySessionBean.class);
        proxySession.setToken("1234");
        proxySession.setTransactionId(txIdMock);
        when(proxySession.getParameterAsString(ProxySessionParameterBean.URL_REDIRECT_PREVIOUS_STEP_UI)).thenReturn(ENDPOINT);
        when(this.proxySessionDataServiceImpl.findSessionByTransactionId(any(String.class))).thenReturn(proxySession);
        when(this.service.uploadProxyResult(any(), any(), any(), any(), any())).thenReturn(Response.ok(true).build());

        // Call callback signature
        this.mvc.perform( //
                get("/public/dictao/callback") //
                        .param("status", CodeRetourDictaoEnum.CANCEL.getCode()) //
                        .param("login", login) //
                        .param("offer", ISignatureConstante.OFFER_TYPE) //
        ) //
                .andExpect(status().is3xxRedirection());
    }

    @Path("/v1/Record")
    public static interface ITestService {

        @POST
        @Path("/code/{recordUid}/step/{stepId}/phase/{processPhase}/process/{processId}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        Response uploadProxyResult(@PathParam("recordUid") String recordUid, @PathParam("stepId") String stepId, @PathParam("processPhase") String processPhase,
                @PathParam("processId") String processId, ProxyResultBean proxyResult);

        @DELETE
        @Path("/code/{code}/{resourcePath:.*}")
        Response remove(@PathParam("code") String code, @PathParam("resourcePath") String resourcePath);
    }

}
