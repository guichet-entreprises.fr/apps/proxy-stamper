package fr.ge.common.proxy.stamper.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyListOf;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.dictao.wsdl.dtp.frontoffice.v5.DocumentPort;
import com.dictao.wsdl.dtp.frontoffice.v5.TransactionPort;
import com.dictao.wsdl.dtp_wcs.v1.ContentPort;
import com.dictao.wsdl.dtp_wcs.v1.CreateUserSpaceRequest;
import com.dictao.wsdl.dtp_wcs.v1.DeleteUserSpaceRequest;
import com.dictao.wsdl.dtp_wcs.v1.EchoRequest;
import com.dictao.wsdl.dtp_wcs.v1.EchoResponse;
import com.dictao.wsdl.dtp_wcs.v1.SystemFaultException;
import com.dictao.wsdl.dtp_wcs.v1.UserFaultException;
import com.dictao.wsdl.dtp_wcs.v1.UserSpacePort;
import com.dictao.xsd.dtp.common.v5.Document;
import com.dictao.xsd.dtp.common.v5.PersonalInfo;
import com.dictao.xsd.dtp.common.v5.SignatureRequest;

import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.stamper.bean.StamperConfigurationBean;
import fr.ge.common.utils.test.AbstractTest;

public class DictaoServiceTest extends AbstractTest {

    /** response from ping. */
    private static final String RESPONSE_FROM_PING = " from User username=\"CN=SAAS INTG GUICHET-ENTREPRISES Client, O=Demo " + "Dictao SA, L=Paris, ST=IDF, C=FR\", roles=\"moe,guichet_entreprises\"";

    /** dictao service. */
    @InjectMocks
    private DictaoService dictaoService;

    /** configuration. */
    @Mock
    private StamperConfigurationBean stamperConfigurationBean;

    /** echo port wcs. */
    @Mock
    private com.dictao.wsdl.dtp_wcs.v1.EchoPort echoPortWcs;

    /** echo port front office. */
    @Mock
    private com.dictao.wsdl.dtp.frontoffice.v5.EchoPort echoPortDtp;

    /**
     * TransactionPort.
     */
    @Mock
    private com.dictao.wsdl.dtp.frontoffice.v5.TransactionPort transactionPortBouchon;

    /**
     * DocumentPort.
     */
    @Mock
    private com.dictao.wsdl.dtp.frontoffice.v5.DocumentPort documentPortBouchon;

    /**
     * EchoPort v5.
     */
    @Mock
    private com.dictao.wsdl.dtp.frontoffice.v5.EchoPort echoPortDtpBouchon;

    /**
     * EchoPort v1.
     */
    @Mock
    private com.dictao.wsdl.dtp_wcs.v1.EchoPort echoPortWcsBouchon;

    /**
     * UserSpacePort.
     */
    @Mock
    private com.dictao.wsdl.dtp_wcs.v1.UserSpacePort userSpacePortBouchon;

    /**
     * ContentPort.
     */
    @Mock
    private com.dictao.wsdl.dtp_wcs.v1.ContentPort contentPortBouchon;

    @Mock
    private TransactionPort transactionPort;

    @Mock
    private DocumentPort documentPort;

    @Mock
    private UserSpacePort userSpacePort;

    @Mock
    private ContentPort contentPort;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(stamperConfigurationBean.getDictaoEndUserUrl()).thenReturn("http://www.google.fr");
        Mockito.when(stamperConfigurationBean.getDictaoEndUserUrlBouchon()).thenReturn(null);
    }

    /**
     * Test create transaction.
     * 
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             UserFaultException
     */
    @Test
    public void testCreateTransaction() throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {

        when(transactionPort.createTransaction("guichet_entreprises", "scnge", "default", "default", null)).thenReturn("testCreateTransaction");
        String value = this.dictaoService.createTransaction();
        assertNotNull(value);

        when(transactionPort.createTransaction("guichet_entreprises", "scnge", "default", "default", null)).thenReturn(null);
        value = this.dictaoService.createTransaction();
        assertNull(value);
    }

    /**
     * Test createContent.
     * 
     * @throws UnsupportedEncodingExceptionUnsupportedEncodingException
     * 
     * @throws UnsupportedEncodingException
     *             UnsupportedEncodingException
     * @throws IOException
     *             IOException
     * @throws UserFaultException
     *             UserFaultException
     * @throws SystemFaultException
     *             SystemFaultException
     */
    @Test
    public void testCreateContent() throws UnsupportedEncodingException, IOException, UserFaultException, SystemFaultException {
        String login = null;
        String userAccessId = null;
        String attributes = null;
        String offerType = null;

        when(contentPortBouchon.create(any(com.dictao.wsdl.dtp_wcs.v1.CreateContentRequest.class))).thenReturn(null);
        this.dictaoService.createContent(login, userAccessId, attributes, offerType);
    }

    /**
     * Test deleteAndFinishUser.
     * 
     * @throws UnsupportedEncodingException
     *             UnsupportedEncodingException
     * @throws IOException
     *             IOException
     * @throws UserFaultException
     *             UserFaultException
     * @throws SystemFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             UserFaultException
     */
    @Test
    public void testDeleteAndFinishUser() throws UnsupportedEncodingException, IOException, UserFaultException, SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException,
            com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {
        String login = null;
        String userAccessId = null;

        when(userSpacePort.delete(any(DeleteUserSpaceRequest.class))).thenReturn(null);
        Mockito.doNothing().when(transactionPort).finishUserAccess(anyString());

        this.dictaoService.deleteAndFinishUser(login, userAccessId);
    }

    /**
     * Test getDocument.
     * 
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             UserFaultException
     */
    @Test
    public void testGetDocument() throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {
        final String txId = null;
        final String documentName = null;

        when(documentPortBouchon.getDocument(anyString(), anyString())).thenReturn(null);
        com.dictao.xsd.dtp.common.v5.Document document = this.dictaoService.getDocument(txId, documentName);
        assertNull(document);

        com.dictao.xsd.dtp.common.v5.Document documentMock = Mockito.mock(com.dictao.xsd.dtp.common.v5.Document.class);
        when(documentPortBouchon.getDocument(anyString(), anyString())).thenReturn(documentMock);
        document = this.dictaoService.getDocument(txId, documentName);

    }

    /**
     * test FinishAndArchive.
     * 
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             UserFaultException
     */
    @Test
    public void testFinishAndArchive() throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {

        final String txId = null;
        final ProxySessionBean proxySessionBean = Mockito.mock(ProxySessionBean.class);
        Mockito.when(proxySessionBean.getParameterAsString("lastName")).thenReturn("lastName");
        Mockito.when(proxySessionBean.getParameterAsString("firstName")).thenReturn("firstName");
        Mockito.when(proxySessionBean.getParameterAsString("email")).thenReturn("email");

        final String typeSignature = null;

        Mockito.doNothing().when(transactionPortBouchon).finishTransaction(anyString());
        Mockito.doNothing().when(transactionPortBouchon).archiveTransaction(anyString(), anyListOf(String.class), any(com.dictao.xsd.dtp.common.v5.ArchiveMetadata.class));

        this.dictaoService.finishAndArchive(txId, proxySessionBean, typeSignature);
    }

    /**
     * Test cancelTransaction.
     * 
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             UserFaultException
     */
    @Test
    public void testCancelTransaction() throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {

        final String txId = null;

        Mockito.doNothing().when(transactionPortBouchon).cancelTransaction(anyString());

        this.dictaoService.cancelTransaction(txId);
    }

    /**
     * Test createUserAccess.
     * 
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             UserFaultException
     */
    @Test
    public void testCreateUserAccess() throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {

        final String txId = null;
        Mockito.doNothing().when(documentPortBouchon).signDocuments(anyString(), anyListOf(SignatureRequest.class));

        when(transactionPortBouchon.createUserAccess(anyString(), any(PersonalInfo.class), anyLong(), any(com.dictao.xsd.dtp.common.v5.Metadata.class), anyListOf(String.class), anyString()))
                .thenReturn(null);

        this.dictaoService.createUserAccess(txId, "M.", "Random", "User", "+336600000000");
    }

    /**
     * Test signServerTransaction.
     * 
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             SystemFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             UserFaultException
     */
    @Test
    public void testSignServerTransaction() throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {

        final String txId = null;
        Mockito.doNothing().when(documentPortBouchon).signDocuments(anyString(), anyListOf(SignatureRequest.class));
        this.dictaoService.signServerTransaction(txId, "Random", "User", "signature");
    }

    /**
     * Test echo wcs.
     *
     * @throws UserFaultException
     *             user fault exception
     * @throws SystemFaultException
     *             system fault exception
     */
    @Test
    public void testEchoWCS() throws UserFaultException, SystemFaultException {
        EchoRequest requestWCS = new EchoRequest();
        String echoMessage = "Hello world";
        String echoResponse = echoMessage + RESPONSE_FROM_PING;

        EchoResponse responseMock = Mockito.mock(EchoResponse.class);
        when(responseMock.getResponse()).thenReturn(echoResponse);
        when(echoPortWcs.echo(requestWCS)).thenReturn(responseMock);

        requestWCS.setRequest(echoMessage);

        EchoResponse response = echoPortWcs.echo(requestWCS);
        assertNotNull(response);
        assertEquals(echoResponse, response.getResponse());
    }

    /**
     * Test echo front office.
     * 
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException
     *             UserFaultException
     * @throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException
     *             SystemFaultException
     */
    @Test
    public void testEchoFrontOffice() throws com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException, com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException {
        String echoMessage = "Hello world";

        when(echoPortDtp.echo(echoMessage)).thenReturn(echoMessage);

        String response = echoPortDtp.echo(echoMessage);

        assertNotNull(response);
        assertEquals(echoMessage, response);
    }

    /**
     * test UserSpace dictao.
     * 
     * @throws UnsupportedEncodingException
     *             UnsupportedEncodingException
     * @throws IOException
     *             IOException
     * @throws UserFaultException
     *             UserFaultException
     * @throws SystemFaultException
     *             SystemFaultException
     */
    @Test
    public void testCreateUserSpace() throws UnsupportedEncodingException, IOException, UserFaultException, SystemFaultException {
        String attributes = null;
        String login = null;

        when(userSpacePortBouchon.create(any(CreateUserSpaceRequest.class))).thenReturn(null);

        this.dictaoService.createUserSpace(attributes, login, "M.", "Random", "User", "random.user@gipge.com");
    }

    @Test
    public void testPutDocument() throws com.dictao.wsdl.dtp.frontoffice.v5.SystemFaultException, com.dictao.wsdl.dtp.frontoffice.v5.UserFaultException {
        String transactionId = null;
        String fileName = "test.pdf";
        String numeroDossier = "DOSSIER_1";
        byte[] fileContent = this.resourceAsBytes("P4_agricole.pdf");
        String zoneIdentifier = "signature";

        doNothing().when(documentPort).putDocument(anyString(), any(Document.class), anyString());
        this.dictaoService.putDocument(transactionId, fileName, numeroDossier, fileContent, zoneIdentifier);

        verify(this.documentPort).putDocument(eq(null), any(), anyString());
    }
}
