package fr.ge.common.proxy.stamper.mock;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.data.service.IProxySessionDataService;
import fr.ge.common.proxy.stamper.bean.StamperConfigurationBean;

public class StamperServiceMockTest {

    /** Le service dictao. */
    @Mock
    private StamperConfigurationBean stamperConfigurationBean;

    @Mock
    private IProxySessionDataService proxySessionDataService;

    @InjectMocks
    private StamperServiceMock stamperService;

    @Before
    public void setUp() throws UnsupportedEncodingException, IOException {
        MockitoAnnotations.initMocks(this);
        Mockito.doNothing().when(this.proxySessionDataService).addTransaction(anyString(), anyString());
        Mockito.when(stamperConfigurationBean.getUrlStamper()).thenReturn("http://localhost:8888/test");
    }

    @Test
    public void testDictaoCall() {
        assertNotNull(this.stamperService.dictaoCall(new ProxySessionBean()));
    }

    @Test
    public void testGetDocument() {
        ProxySessionBean session = Mockito.mock(ProxySessionBean.class);
        Mockito.when(proxySessionDataService.findSessionByTransactionId(anyString())).thenReturn(session);
        Mockito.when(session.getParameterAsBytes("files.1.doc")).thenReturn("CONTENT".getBytes());
        assertNotNull(this.stamperService.getDocument("txId", "documentName"));
    }

    @Test
    public void testDeleteAndFinishUser() {
        this.stamperService.deleteAndFinishUser("txId", "accessId");
    }

    @Test
    public void testFinishAndArchive() {
        this.stamperService.finishAndArchive("txId", new ProxySessionBean(), "offer");
    }

    @Test
    public void testCancelTransaction() {
        this.stamperService.cancelTransaction("txId");
    }

    @Test
    public void testCreateTransaction() {
        assertNotNull(this.stamperService.createTransaction());
    }
}
