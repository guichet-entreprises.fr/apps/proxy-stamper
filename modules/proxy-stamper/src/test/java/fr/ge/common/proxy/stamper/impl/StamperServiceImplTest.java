package fr.ge.common.proxy.stamper.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.common.proxy.data.bean.ProxySessionBean;
import fr.ge.common.proxy.data.service.IProxySessionDataService;
import fr.ge.common.proxy.stamper.bean.StamperConfigurationBean;

/**
 * Testing Stamper implementation.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class StamperServiceImplTest {

    /** Le service dictao. */
    @Mock
    private StamperConfigurationBean stamperConfigurationBean;

    @Mock
    private IProxySessionDataService proxySessionDataService;

    /** Le service dictao. */
    @Mock
    private DictaoService dictaoService;

    @InjectMocks
    private StamperServiceImpl stamperService;

    @Before
    public void setUp() throws UnsupportedEncodingException, IOException {
        MockitoAnnotations.initMocks(this);
        Mockito.when(stamperConfigurationBean.getDictaoEndUserUrl()).thenReturn("http://www.google.fr");
        Mockito.when(stamperConfigurationBean.getDictaoEndUserUrlBouchon()).thenReturn(null);
        Mockito.when(stamperConfigurationBean.getUrlStamper()).thenReturn("http://www.google.fr");
        Mockito.doNothing().when(this.proxySessionDataService).addTransaction(Mockito.anyString(), Mockito.anyString());
        Mockito.when(dictaoService.createUserAccess(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("accessId");
        Mockito.when(this.dictaoService.createTransaction()).thenReturn("1");
        Mockito.doNothing().when(this.dictaoService).putDocument(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(), Mockito.anyString());
        Mockito.doNothing().when(this.dictaoService).signServerTransaction(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
        Mockito.doNothing().when(this.dictaoService).createContent(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void testDictaoCall() {
        ProxySessionBean session = Mockito.mock(ProxySessionBean.class);
        Mockito.when(session.getParameterAsString("lastName")).thenReturn("lastName");
        Mockito.when(session.getParameterAsString("firstName")).thenReturn("firstName");
        Mockito.when(session.getParameterAsString("email")).thenReturn("email");
        Mockito.when(session.getParameterAsBytes("files.1.doc")).thenReturn("CONTENT".getBytes());
        Mockito.when(session.getParameterAsString("files.1.zoneId")).thenReturn("signature");

        Mockito.when(this.dictaoService.createTransaction()).thenReturn("1");

        this.stamperService.dictaoCall(session);
    }
}
