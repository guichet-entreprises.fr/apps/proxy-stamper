package fr.ge.common.proxy.stamper.core.form.impl.expr;

import java.util.regex.Pattern;

/**
 * Remplace les patterns formalite[ X ] par formalite.X;
 * 
 * @author Nicolas Richeton
 * 
 */
public class FormaliteStaticReplace extends GenericReplace {
  /**
   * formalite\\[([A-Za-z0-9_\\.]+)\\]
   */
  private static final Pattern BEAN_REF = Pattern.compile("formalite\\[([A-Za-z0-9_\\.]+)\\]");

  /**
   * Constructeur de classe.
   */
  public FormaliteStaticReplace() {
    super(BEAN_REF, "formalite.$1");
  }

}
