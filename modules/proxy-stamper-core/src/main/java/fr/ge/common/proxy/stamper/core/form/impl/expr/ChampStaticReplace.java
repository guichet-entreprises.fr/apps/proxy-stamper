package fr.ge.common.proxy.stamper.core.form.impl.expr;

/**
 * Remplace les patterns champ[ X ] par courant.X;
 * 
 * @author Nicolas Richeton
 * 
 */
public class ChampStaticReplace extends AbstractChampReplace {

  /**
   * Constructeur de classe.
   */
  public ChampStaticReplace() {
    super(FORM_REF, "courant.$1");
  }

}
