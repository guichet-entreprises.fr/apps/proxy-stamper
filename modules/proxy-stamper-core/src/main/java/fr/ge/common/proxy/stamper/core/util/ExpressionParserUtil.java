package fr.ge.common.proxy.stamper.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.Expression;
import org.springframework.expression.ParseException;
import org.springframework.expression.common.TemplateAwareExpressionParser;

/**
 * Le Class ExpressionParserUtil.
 */
public final class ExpressionParserUtil {

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = LoggerFactory.getLogger(ExpressionParserUtil.class);

    /**
     * Private constructor for utility class.
     */
    private ExpressionParserUtil() {
        // Nothing to do.
    }

    /**
     * Parser.
     *
     * @param id
     *            le id
     * @param groupe
     *            le groupe
     * @param parser
     *            le parser
     * @param expressionAvantPerform
     *            le expression avant perform
     * @param expressionApresPerform
     *            le expression apres perform
     * @return le expression
     */
    public static Expression parser(final String id, final String groupe, final TemplateAwareExpressionParser parser, final String expressionAvantPerform, final String expressionApresPerform) {
        try {
            return parser.parseExpression(expressionApresPerform);
        } catch (ParseException e) {
            String messageErreurValidation = construireMessageValidation(id, groupe, expressionAvantPerform, expressionApresPerform);
            throw new IllegalArgumentException(messageErreurValidation, e);
        } catch (IllegalStateException e) {
            String messageErreurValidation = construireMessageValidation(id, groupe, expressionAvantPerform, expressionApresPerform);
            throw new IllegalArgumentException(messageErreurValidation, e);
        }
    }

    /**
     * Construire message validation.
     *
     * @param id
     *            le id
     * @param groupe
     *            le groupe
     * @param expressionAvantPerform
     *            le expression avant perform
     * @param expressionApresPerform
     *            le expression apres perform
     * @return le string
     */
    private static String construireMessageValidation(final String id, final String groupe, final String expressionAvantPerform, final String expressionApresPerform) {

        StringBuilder sb = new StringBuilder("Erreur lors de la validation de l'élément : ID = ");
        sb.append(id);
        sb.append(" GROUPE=");
        sb.append(groupe);
        sb.append(".\nL'expression invalide est la suivante : ");
        sb.append(expressionAvantPerform);

        LOGGER_TECH.debug("{} Expression après traduction {}", id, expressionApresPerform);
        return sb.toString();
    }
}
