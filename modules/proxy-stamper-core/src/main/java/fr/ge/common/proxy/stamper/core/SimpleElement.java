package fr.ge.common.proxy.stamper.core;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Représente un élément Formiz.
 * <p>
 * Ajoute les attributs nécessaire modèlisation d'un élément :
 * <ul>
 * <li>Identifiant</li>
 * <li>groupe d'appartenance</li>
 * </ul>
 * 
 */
public abstract class SimpleElement extends AbstractSimpleElement {

    /** Le parent. */
    private FormMetadata parent;

    /**
     * Constructeur de classe. Il définit les attribut nécessaire à la création
     * d'un élément quelconque
     * 
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments
     */
    public SimpleElement(FormMetadata parent, String id, String group) {
        super(group, id);
        this.parent = parent;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le parent.
     *
     * @return the parent
     */
    public FormMetadata getParent() {
        return parent;
    }

    /**
     * Set le parent.
     *
     * @param parent
     *            the parent to set
     */
    public void setParent(FormMetadata parent) {
        this.parent = parent;
    }

}
