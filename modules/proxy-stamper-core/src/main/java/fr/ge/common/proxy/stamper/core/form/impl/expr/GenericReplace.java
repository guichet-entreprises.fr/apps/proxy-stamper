package fr.ge.common.proxy.stamper.core.form.impl.expr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Remplace des occurences dans une chaine de caractère.
 * <p>
 * Conserve la liste des éléments remplacés peut être récupérée avec {@link #getMatchedItems()}.
 * 
 * <p>
 * Il est possible de surcharger la méthode {@link #onMatch(String)} pour retraiter les éléments
 * avant ajout dans la liste des références.
 * 
 * 
 * @author Nicolas Richeton
 * 
 */
public class GenericReplace {

  /**
   * Map indiquant le remplacement pour une chaine.
   */
  private Map < String, String > items;

  /**
   * Chaine pour lesquelles il a été effectué un remplacement.
   */
  private List < String > matchedItems;

  /**
   * Le pattern à remplacer.
   */
  private final Pattern pattern;

  /**
   * Chaine qui a remplacer le pattern.
   */
  private List < String > replacedItems;

  /** Chaine de remplacement. */
  private final String replacement;

  /**
   * Constructeur de classe.
   * 
   * @param pattern
   *          pattern qui doit être remplacé
   * @param replacement
   *          la chaine de carractère qui doit remplacer le pattern
   */
  public GenericReplace(Pattern pattern, String replacement) {
    this.pattern = pattern;
    this.replacement = replacement;
  }

  /**
   * Getter de l'attribut items.
   * 
   * @return la valeur de items
   */
  public Map < String, String > getItems() {
    return items;
  }

  /**
   * Getter de l'attribut matchedItems.
   * 
   * @return la valeur de matchedItems
   */
  public List < String > getMatchedItems() {
    return matchedItems;
  }

  /**
   * Getter de l'attribut replacedItems.
   * 
   * @return la valeur de replacedItems
   */
  public List < String > getReplacedItems() {
    return replacedItems;
  }

  /**
   * Execute le remplacement.
   * 
   * @param expr
   *          expression a transformer
   * @return la nouvelle expression
   */
  public String perform(String expr) {
    if (expr == null) {
      return null;
    }

    matchedItems = new ArrayList < String >();
    replacedItems = new ArrayList < String >();
    items = new HashMap < String, String >();

    StringBuffer newValuesBuffer = new StringBuffer();
    Matcher m = pattern.matcher(expr);
    while (m.find()) {
      String grp = m.group(1);
      String newGrp = replacement.replace("$1", grp);
      matchedItems.add(grp);
      replacedItems.add(newGrp);
      items.put(grp, newGrp);
      m.appendReplacement(newValuesBuffer, replacement);
    }
    m.appendTail(newValuesBuffer);
    return newValuesBuffer.toString();
  }
}
