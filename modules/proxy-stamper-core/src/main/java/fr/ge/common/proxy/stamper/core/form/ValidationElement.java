package fr.ge.common.proxy.stamper.core.form;

import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;

import fr.ge.common.proxy.stamper.core.FormMetadata;

/**
 * Elément modèlisant une règle de validation.
 * <p>
 * Les validateurs classique utilisé dans un formulaire :
 * <p>
 * <ul>
 * <li>Courriel</li>
 * <li>Format - le champ doit repecté un regex passée en paramètre</li>
 * <li>Max - Le champ doit etre de longueur inférieur à la taille passée en paramètre</li>
 * <li>Min - Le champ doit etre de longueur supérieur à la taille passée en paramètre</li>
 * <li>Range - La valeur du champ doit être comprise en les bornes passées en paramètre</li>
 * <li>Telephone - Il peut commencer par 0, 0033 ou +33</li>
 * <li>Date - JJ/MM/AAAA par défaut. Le format peut être passé en paramètre</li>
 * <li>Obligatoire</li>
 * </ul>
 * 
 * @author Nicolas Richeton
 * 
 */
public class ValidationElement extends SimpleFormElement {

  /** La constante COURRIEL. */
  public static final String COURRIEL = "courriel"; 

  /** La constante FORMAT. */
  public static final String FORMAT = "format"; 

  /** La constante MAX. */
  public static final String MAX = "max"; 

  /** La constante MIN. */
  public static final String MIN = "min"; 

  /** La constante RANGE. */
  public static final String RANGE = "range"; 

  /** La constante SECU. */
  public static final String SECU = "numerosecu"; 

  /** La constante SECU_AYANT_DROIT. */
  public static final String SECU_AYANT_DROIT = "numerosecuayantdroit"; 

  /** La constante SECU_CONJOINT. */
  public static final String SECU_CONJOINT = "numerosecuconjoint"; 

  /** La constante SIREN. */
  public static final String SIREN = "siren"; 

  /** La constante DATE_NAISSANCE. */
  public static final String DATE_NAISSANCE = "datenaissance"; 

  /** La constante CODE_POSTAL_DESTINATAIRE_CFE. */
  public static final String CODE_POSTAL_DESTINATAIRE_CFE = "codepostaldestinatairecfe";

  /** La constante CODE_POSTAL_DIRIGEANT_DESTINATAIRE_CFE. */
  public static final String CODE_POSTAL_DIRIGEANT_DESTINATAIRE_CFE = "codepostaldirigeantdestinatairecfe";

  /** La constante PARSLEY_PAYS. */
  public static final String PAYS = "pays"; 

  /** La constante SIRET. */
  public static final String SIRET = "siret"; 

  /** La constante TEL. */
  public static final String TEL = "telephone"; 

  /** La constante APE. */
  public static final String APE = "codeape"; 
  // N'est pas utilisé pour les ids de validateur mais les validateurs coté
  /** La constante DATE. */
  // serveur.
  public static final String DATE = "date"; 

  /** La constante MIN_DATE. */
  public static final String MIN_DATE = "mindate"; 

  /** La constante MAX_DATE. */
  public static final String MAX_DATE = "maxdate"; 

  /** La constante OBLIGATOIRE. */
  public static final String OBLIGATOIRE = "obligatoire"; 

  /** Le param. */
  private String param = null;
  // Le param compilé doit uniquement être utilisé pour précompiler les
  /** Le param compile. */
  // validateurs de type Regexp.
  private Pattern paramCompile = null;

  /** Le valid ids. */
  private final String[] validIds = new String[] {FORMAT, RANGE, SIREN, CODE_POSTAL_DESTINATAIRE_CFE,
    CODE_POSTAL_DIRIGEANT_DESTINATAIRE_CFE, PAYS, DATE_NAISSANCE, SECU, SECU_AYANT_DROIT, SECU_CONJOINT, MIN, MAX, COURRIEL, TEL,
    OBLIGATOIRE, DATE, MIN_DATE, MAX_DATE, SIRET, APE };

  /**
   * Constructeur de classe.
   * 
   * @param parent
   *          les méta données du formulaire contenant l'élément
   * @param id
   *          identifiant de la règle de validation
   * @param group
   *          groupe d'appartenance de l'éléments
   * @param param
   *          les paramètre du type
   * @param visibility
   *          expression de la visibilité de la règle de validation
   */
  public ValidationElement(FormMetadata parent, String id, String group, String param, String visibility) {
    super(parent, id, group, visibility);

    if (!ArrayUtils.contains(this.validIds, id)) {
      throw new IllegalArgumentException("Règle de validation invalide: " 
        + id);
    }
    this.param = param;
    if (FORMAT.equals(id)) {
      if (param == null) {
        this.paramCompile = Pattern.compile(".*"); 
      } else {
        this.paramCompile = Pattern.compile(param);
      }
    }
  }

  /**
   * Getter de l'attribut param.
   * 
   * @return la valeur de param
   */
  public String getParam() {
    return this.param;
  }

  /**
   * Getter de l'attribut paramCompile.
   * 
   * @return la valeur de paramCompile
   */
  public Pattern getParamCompile() {
    return this.paramCompile;
  }

}
