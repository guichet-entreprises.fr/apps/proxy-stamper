package fr.ge.common.proxy.stamper.core.form.impl.expr;

import org.apache.commons.lang3.StringUtils;

/**
 * Le Class NullSafeStaticReplace.
 */
public class NullSafeStaticReplace {

  /**
   * Perform.
   *
   * @param expr
   *          le expr
   * @return le string
   */
  public String perform(String expr) {
    String result = StringUtils.EMPTY;
    if (StringUtils.isNotBlank(expr)) {
      result = expr.replaceAll("\\.", "?.");
    }
    return result;
  }
}
