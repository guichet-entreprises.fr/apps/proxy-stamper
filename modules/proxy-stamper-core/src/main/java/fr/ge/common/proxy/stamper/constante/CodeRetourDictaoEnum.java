package fr.ge.common.proxy.stamper.constante;

/**
 * Enum CodeRetourDictaoEnum.
 */
public enum CodeRetourDictaoEnum {

  /** ok. */
  OK("OK", "Sortie en succès"),

  /** cancel. */
  CANCEL("CANCEL", "Accès utilisateur annulé par l’utilisateur final"),

  /** err user. */
  ERR_USER("ERR_USER", "Erreur liée à une mauvaise initialisation du contexte applicatif (ex : paramètres d'entrée invalides)"),

  /** err user auth. */
  ERR_USER_AUTH("ERR_USER_AUTH", "Erreur liée à une authentification incorrecte"),

  /** err environment. */
  ERR_ENVIRONMENT("ERR_ENVIRONMENT", "Erreur liée à l'environnement d'exécution de DTP " + "(ex : service tiers injoignable)"),

  /** err environment session expired. */
  ERR_ENVIRONMENT_SESSION_EXPIRED("ERR_ENVIRONMENT_SESSION_EXPIRED", "Erreur retournée lorsque la session de l’utilisateur sur l’IHM a expiré ERR_INTERNAL Erreur inattendue pouvant nécessiter un correctif");

  /** Le code. */
  private String code;

  /** Le colonne. */
  private String description;

  /**
   * Constructeur.
   * 
   * @param code
   *          code retour
   * @param description
   *          : description
   */
  CodeRetourDictaoEnum(final String code, final String description) {
    this.code = code;
    this.description = description;
  }

  /**
   * Accesseur sur l'attribut code.
   *
   * @return code
   */
  public String getCode() {
    return this.code;
  }

  /**
   * Accesseur sur l'attribut description.
   *
   * @return description
   */
  public String getDescription() {
    return this.description;
  }
}
