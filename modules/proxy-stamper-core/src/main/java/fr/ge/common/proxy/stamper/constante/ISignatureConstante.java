/**
 * 
 */
package fr.ge.common.proxy.stamper.constante;

/**
 * Properties par défaut - Signature.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public interface ISignatureConstante {

    /** dictao.dtp.tenant_name. **/
    String CT_DICTAO_DPT_TENANT_NAME = "guichet_entreprises";

    /** dictao.dtp.policy : à vérifier si c'est utilisé quelque part. **/
    String DICATO_DPT_POLICY = "scnge";

    /**
     * dictao.echo.checkString=@@dictao.echo.checkString@@ : Hello world from
     * User username.
     **/
    String CT_DICTAO_ECH0_CHECK_STRING = "Hello world from User username";

    /**
     * dictao.indicatif.internationnal=@@dictao.indicatif.internationnal@@ : 00
     * .
     **/
    String CT_DICTAO_INDICATIF_INTERNATIONAL = "00";

    /**
     * dictao.dtp.frontoffice.backEndBaseUrl=@@dictao.dtp.frontoffice.backEndBaseUrl@@
     * : https://ws1-intg.dictao.com/ : @TODO à vérifier si c'est utilisé .
     **/
    String CT_DICTAO_DTP_FRONTOFFICE_BACK_END_BASE_URL = "https://ws1-intg.dictao.com/";

    /**
     * ct.dictao.dtp.frontoffice.endUserBaseUrl=https://service1-intg.dictao.com/
     * : @TODO : à vérifier si c'est utilisé ou pas.
     **/
    String CT_DICTAO_DTP_FRONTOFFICE_END_USER_BASE_URL = "https://service1-intg.dictao.com/";

    /**
     * dictao.dtp.ws.url=https://ws1-intg.dictao.com/dtp/frontoffice/ws/v5u1.
     **/
    // String CT_DICTAO_DPT_WS_URL =
    // "https://ws1-intg.dictao.com/dtp/frontoffice/ws/v5u1";

    /** dictao.wcs.ws.url=https://ws1-intg.dictao.com/dtp-wcs/ws. **/
    String CT_DICTAO_WCS_WS_URL = "https://ws1-intg.dictao.com/dtp-wcs/ws";

    /**
     * dictao.wcs.euapp.url=https://service1-intg.dictao.com/dtp-ui/guichet_entreprises/scnge/#.
     **/
    String CT_DICTAO_WCS_EUAPP_URL = "https://service1-intg.dictao.com/dtp-ui/guichet_entreprises/scnge/#";

    /**
     * dictao.wcs.euapp.url.bouchon=https://service1-intg.dictao.com/dtp-ui/guichet_entreprises/scnge/#.
     **/
    String CT_DICTAO_WCS_EUAPP_URL_BOUCHON = "https://service1-intg.dictao.com/dtp-ui/guichet_entreprises/scnge/#";

    /**
     * dictao.dtp.ws.url.bouchon=https://ws1-intg.dictao.com/dtp/frontoffice/ws/v5u1.
     **/
    String CT_DICTAO_DPT_WS_URL_BOUCHON = "https://ws1-intg.dictao.com/dtp/frontoffice/ws/v5u1";

    /** dictao.wcs.ws.url.bouchon=https://ws1-intg.dictao.com/dtp-wcs/ws. **/
    String CT_DICTAO_WCS_WS_URL_BOUCHON = "https://ws1-intg.dictao.com/dtp-wcs/ws";

    /**
     * ct.dictao.ssl.keyStore=dictao/certificates/User/saas-intg-guichet-entreprises-client.p12.
     **/
    String CT_DICTAO_SSL_KEYSTORE = "dictao/certificates/User/saas-qa-dtp-guichet_entreprises-client.p12";

    /** ct.dictao.ssl.keyStorePassword=PNTkC7kZ. **/
    String CT_DICTAO_SSL_KEY_STORE_PASSWORD = "8EDcQ85h";

    /** dictao.ssl.keyStoreType=PKCS12. **/
    String CT_DCTAO_SSL_KEY_STORE_TYPE = "PKCS12";

    /**
     * dictao.ssl.trustStore=dictao/certificates/SSL/thawte_Primary_Root_CA.jks.
     **/
    String CT_DICTAO_SSL_TRUST_STORE = "dictao/certificates/SSL/saas-qa-dtp-guichet_entreprises-client.jks";

    /** dictao.ssl.trustStorePassword=YKxL9m2V. **/
    String CT_DICTAO_SSL_TRUST_STORE_PASSWORD = "YKxL9m2V";

    /** dictao.ssl.trustStoreType=JKS. **/
    String CT_DICTAO_SSL_TRUST_STORE_TYPE = "JKS";

    /** Le motif de laision des paramètres login. */
    String MOTIF_SEP_LOGIN = "_";

    /** Le nom de l'offre cessation. */
    String OFFER_TYPE = "formalite";
}
