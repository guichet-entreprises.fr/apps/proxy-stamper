package fr.ge.common.proxy.stamper.core.form.impl.expr;

/**
 * Remplace les patterns champ[ X ] par #ajax("X");.
 *
 * @author Nicolas Richeton
 */
public class ChampAjaxReplace extends AbstractChampReplace {

  /** La constante STRING_FORM_REF_STATIC. */
  private static final String STRING_FORM_REF_STATIC = "@ajax[\"$1\"]"; 

  /**
   * Constructeur de classe.
   */
  public ChampAjaxReplace() {
    super(FORM_REF, STRING_FORM_REF_STATIC);
  }

}
