package fr.ge.common.proxy.stamper.core.form.impl.expr;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * Permet la compilation lors de l'utilisation des beans externes
 * </p>
 * .
 *
 * @author JISAVI
 */
public class BeansExternesStaticReplace {

  /** Liste des id de beans externes à un flow. */
  private List < String > listeIdsBeansExternes;

  /**
   * Exemple : "profil[activiteAgentCommercial] ([A-Za-z0-9_\\.]+)\\[([A-Za-z0-9_\\.]+)\\]
   */

  private static final Pattern BEANS_EXTERNES_REF = Pattern.compile("([A-Za-z0-9_\\.]+)\\[([A-Za-z0-9\\?\\[\\]_\\.#\\+*]+)\\]");

  /** La constante FORM_REF_BEANS_EXTERNES_STATIC. */
  protected static final String FORM_REF_BEANS_EXTERNES_STATIC = "#recupererBeanExterne('$1',#numeroDossier).$2";

  /**
   * <p>
   * transformation des beans externes , par exemple profil[activiteAgentCommercial] devient
   * recupererBeanExterne('profil').activiteAgentCommercial
   * 
   * </p>
   *
   * @param expr
   *          le expr
   * @return expression parsée
   */
  public String perform(String expr) {
    // tests
    this.construireIdBeansExternes();
    Matcher m = BEANS_EXTERNES_REF.matcher(expr);
    StringBuffer sb = new StringBuffer();
    while (m.find()) {
      if (this.listeIdsBeansExternes.contains(m.group(1))) {
        m.appendReplacement(sb, this.getFormBeanExterneStatic());
      }
    }
    m.appendTail(sb);
    return sb.toString();
  }

  /**
   * Construire id beans externes.
   */
  private void construireIdBeansExternes() {
    this.listeIdsBeansExternes = new ArrayList < String >();
    this.listeIdsBeansExternes.add("cfe");
    this.listeIdsBeansExternes.add("profil");
    this.listeIdsBeansExternes.add("cfeR");
    this.listeIdsBeansExternes.add("cfeC");
    this.listeIdsBeansExternes.add("profilC");
    this.listeIdsBeansExternes.add("profilR");
    this.listeIdsBeansExternes.add("profilM");
    this.listeIdsBeansExternes.add("demande_licence_demande_debit_boisson");
  }

  /**
   * Set le liste ids beans externes.
   *
   * @param listeIdsBeansExternes
   *          le nouveau liste ids beans externes
   */
  public void setListeIdsBeansExternes(List < String > listeIdsBeansExternes) {
    this.listeIdsBeansExternes = listeIdsBeansExternes;
  }

  /**
   * Get le form bean externe static.
   *
   * @return le form bean externe static
   */
  protected String getFormBeanExterneStatic() {
    return FORM_REF_BEANS_EXTERNES_STATIC;
  }

}
