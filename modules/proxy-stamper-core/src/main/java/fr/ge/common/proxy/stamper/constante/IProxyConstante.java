/**
 * 
 */
package fr.ge.common.proxy.stamper.constante;

/**
 * Properties for proxy.
 * 
 * @author aolubi
 * @version $Revision: 0 $
 */
public interface IProxyConstante {

    /** PROXY_SIGNED_DOCUMENT. **/
    String PROXY_SIGNED_DOCUMENT = "PROXY_SIGNED_DOCUMENT.pdf";
}
