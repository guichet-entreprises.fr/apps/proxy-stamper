package fr.ge.common.proxy.stamper.core.form.impl.expr;

import java.util.regex.Pattern;

/**
 * Remplace les patterns courant[ X ] par courant.X;
 * 
 * @author Nicolas Richeton
 * 
 */
public class CourantStaticReplace extends GenericReplace {
  /**
   * courant\\[([A-Za-z0-9_\\.]+)\\]
   */
  protected static final Pattern CURRENT_BEAN_REF = Pattern.compile("courant\\[([A-Za-z0-9_\\.]+)\\]");

  /**
   * Constructeur de classe.
   */
  public CourantStaticReplace() {
    super(CURRENT_BEAN_REF, "courant.$1");
  }

}
