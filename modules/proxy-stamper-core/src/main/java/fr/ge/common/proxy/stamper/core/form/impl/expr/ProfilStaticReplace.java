package fr.ge.common.proxy.stamper.core.form.impl.expr;

import java.util.regex.Pattern;

/**
 * Remplace les patterns profil[ X ] par profil.X;
 * 
 */
public class ProfilStaticReplace extends GenericReplace {
  /**
   * profil\\[([A-Za-z0-9_\\.]+)\\]
   */
  private static final Pattern PROFIL_BEAN_REF = Pattern.compile("profilCfe\\[([A-Za-z0-9_\\.]+)\\]");

  /**
   * Constructeur de classe.
   */
  public ProfilStaticReplace() {
    super(PROFIL_BEAN_REF, "mapBeansExternes[profilCfe].$1");
  }

}
