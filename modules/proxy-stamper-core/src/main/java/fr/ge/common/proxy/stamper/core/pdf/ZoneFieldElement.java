package fr.ge.common.proxy.stamper.core.pdf;

/**
 * @author bsadil
 * 
 *         cette class permet de définir la zone d'un élément pdf
 *
 */
public class ZoneFieldElement {

  /**
   * le numéro de la page ou on trouve l'element du pdf
   */
  private int page;
  /**
   * l'abscisse du l'élément du pdf
   */
  private float abscisse;
  /**
   * l'ordonne de l'élément du pdf
   */
  private float ordonne;
  /**
   * la largeur de l'élément du pdf
   */
  private float largeur;
  /**
   * la hauteur de l'élément du pdf
   */
  private float hauteur;

  /**
   * constructeur de la class ZoneFielElement
   * 
   * @param page
   *          le numéro de la page ou on trouve l'element du pdf
   * @param abscisse
   *          l'abscisse du l'élément du pdf
   * @param ordonne
   *          l'ordonne de l'élément du pdf
   * @param largeur
   *          la largeur de l'élément du pdf
   * @param hauteur
   *          la hauteur de l'élément du pdf
   */
  public ZoneFieldElement(int page, float abscisse, float ordonne, float largeur, float hauteur) {
    super();
    this.page = page;
    this.abscisse = abscisse;
    this.ordonne = ordonne;
    this.largeur = largeur;
    this.hauteur = hauteur;
  }

  /**
   * @return le numéro de la page
   */
  public int getPage() {
    return page;
  }

  /**
   * @return l'abscisse
   */
  public float getAbscisse() {
    return abscisse;
  }

  /**
   * @return l'ordonne du pdf
   */
  public float getOrdonne() {
    return ordonne;
  }

  /**
   * @return la largeur de la zone de l'élément du pdf
   */
  public float getLargeur() {
    return largeur;
  }

  /**
   * @return la hauteur de la zone de l'éléménet du pdf
   */
  public float getHauteur() {
    return hauteur;
  }

  /**
   * @param page
   *          numéro de la page de l'élément du pdf
   */
  public void setPage(int page) {
    this.page = page;
  }

  /**
   * @param abscisse
   *          l'abscisse de l'élément du pdf
   */
  public void setAbscisse(float abscisse) {
    this.abscisse = abscisse;
  }

  /**
   * @param ordonne
   *          l'ordonne de l'élément du pdf
   */
  public void setOrdonne(float ordonne) {
    this.ordonne = ordonne;
  }

  /**
   * @param largeur
   *          la largeur de l'élément du pdf
   */
  public void setLargeur(float largeur) {
    this.largeur = largeur;
  }

  /**
   * @param hauteur
   *          la hauteur de l'élément du pdf
   */
  public void setHauteur(float hauteur) {
    this.hauteur = hauteur;
  }

}
